<?php
require 'headerEleve.php';
?>

<h1>Evaluation d'oral anonyme et instantanée</h1>
EvalOral est une application gratuite qui vous permet d'avoir une évaluation instantannée de vos élèves / étudiants qui passent un oral.
<br><br>
Les autres élèves votent pour celui qui vient de réaliser son oral, et vous obtenez instantanément les résultats.
<br><br>
<h2>Comment ca marche ?</h2>
1- Vous devez tout d'abord créer un compte gratuit, puis ajouter vos élèves sur l'application.<br><br>
2- Ensuite, créez vos grilles d'évaluation composée de ses critères<br><br>
3- Enfin, créez une nouvelle évaluation pour votre classe, basée sur vos critères<br><br>
4- Pendant la séance d'évaluation, les élèves qui assistent à l'oral devront utiliser leur smartphone ou un ordinateur fixe et se rendre sur l'interface élève pour noter leur camarade
<br><br>
<img class="image" src="img/votants.png" alt="votants">
<h5>Pré requis</h5>
<p >Vous devez disposer d'un smartphone/ordinateur par élève votant, et d'une connexion Internet<br>
<div class="card" style="width: 18rem;">
    <a href="https://expo.io/artifacts/63975e3d-1a1b-452f-88c9-75414c91b9ee" target="blank"><img class="card-img-top" width="" src="./img/android.png" alt="Application Android"></a>
  <div class="card-body">
    <h5 class="card-title">Application Android</h5>
    <p class="card-text">Les élèves peuvent s'ils le souhaintent utiliser <a href="https://expo.io/artifacts/63975e3d-1a1b-452f-88c9-75414c91b9ee" target="blank">cette application Android </a>pour répondre, ou utiliser directement le site web</p>
  </div>
</div>
Nb : Une application IOS devrait être prochainement disponible</p>
<br><br>
<h2>FAQ</h2>
1- Qui suis-je ? : Julien Donet, enseignant informatique actuellement en poste en Polynésie Française<br>
2- Pourquoi cette application ? : Pour répondre à un besoin personnel initialement, puis pour partager cette application avec mes collègues, car l'équivalent n'existe pas à ma connaissance sur le marché<br>
3- Est-ce vraiment gratuit ? : Tant que je n'ai pas 1000 connexions/jours sur l'application, l'hébergement ne me coute pas trop cher. Pas de raison de chercher un modèle économique pour l'instant<br>
4- Que deviennent les données collectées ? : Les données prof/élève ne sont pas utilisées à des fins de prospection commerciale ou autres. Pas revendues à des tiers...<br>
5- Il y a un bug, ou une fonctionnalité à ajouter : Merci de me la faire remonter via le mail suivant : <a href="mailto:incoming+jdonet/evaluation@incoming.gitlab.com">incoming+jdonet/evaluation@incoming.gitlab.com</a><br>
<?php
require 'footer.php';
?>

