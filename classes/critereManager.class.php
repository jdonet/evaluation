<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les criteres 
 .
 *
 * @author Julien
 */
class critereManager {
    
    private $db;
    
    /**
     * Instancie un objet critereManager.
     * 
     * Permet d'instanicer un objet critereManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie le critere dans la base.
     * 
     * Pour enregistrer le critere passé en paramètre en base de données :
     *      UPDATE si le critere est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param produit Eleveent à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id du critere ajouté ou mis à jour.
     */
    public function save(critere $crit)
    {        
        
        $nbRows = 0;

        // le critere que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($crit->getId()!=''){
            $query = "select count(*) as nb from `eval_critere` where `idCritere`=?";
            $traitement = $this->db->prepare($query);
            $param1=$crit->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si le critere que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `eval_critere` set `nomCritere`=?, `nbPointsMaxCritere`=?, `archiveCritere`=?, `refGrille`=?, `refEvaluation`=? where `idCritere`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$crit->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$crit->getPointMax();
            $traitement->bindparam(2,$param2);
            $param3=$crit->getArchive();
            $traitement->bindparam(3,$param3);
            $param4=is_null($crit->getGrille())?null:$crit->getGrille()->getId(); //s'il est pas null, on met l'id
            $traitement->bindparam(4,$param4);
            $param5=is_null($crit->getEvaluation())?null:$crit->getEvaluation()->getId(); //s'il est pas null, on met l'id
            $traitement->bindparam(5,$param5);
            $param6=$crit->getId();
            $traitement->bindparam(6,$param6);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `eval_critere` (`nomCritere`, `nbPointsMaxCritere`,`archiveCritere`,`refGrille`, `refEvaluation`) values (?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$crit->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$crit->getPointMax();
            $traitement->bindparam(2,$param2);
            $param3=$crit->getArchive();
            $traitement->bindparam(3,$param3);
            $param4=is_null($crit->getGrille())?null:$crit->getGrille()->getId(); //s'il est pas null, on met l'id
            $traitement->bindparam(4,$param4);
            $param5=is_null($crit->getEvaluation())?null:$crit->getEvaluation()->getId(); //s'il est pas null, on met l'id
            $traitement->bindparam(5,$param5);
            $traitement->execute();
        }
        
        if ($crit->getId() == "")
        {
            $crit->setId($this->db->lastInsertId());
        }
        return $crit->getId();
    }

    /**
     * Supprime le critere de la base.
     * 
     * Supprime de la base le critere (table "critere"), les commandes (table "commande") passées par celui-ci et les lignes de commandes associées (table "comporter").
     * 
     * @param produit Objet critere devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(critere $crit)
    {
        
        $nbRows = 0;

        // le critere que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($crit->getId()!=''){                    
            $query = "select count(*) as nb from `eval_critere` where `idCritere`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $crit->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI le critere que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM comporter, commande, critere
        //          et retourne TRUE
        if ($nbRows > 0)
        {
            $query = "DELETE FROM eval_critere WHERE idCritere=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $crit->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) critere(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de critere correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les criteres.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) critere.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `eval_critere` ".$restriction." order by nomCritere;";
        $critList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            $grilleManager = new grilleManager ($this->db);
            $grille=null;
            if (!is_null(h($row['refGrille'])))
                $grille = $grilleManager->get(h($row['refGrille']));
            $evaluationManager = new evaluationManager ($this->db);
            $evaluation=null;
            if (!is_null(h($row['refEvaluation'])))
                $evaluation = $evaluationManager->get(h($row['refEvaluation']));
            //appel du constructeur paramétré
            $crit = new critere(h($row['nomCritere']),h($row['nbPointsMaxCritere']),h($row['archiveCritere']),$grille,$evaluation);
            //positionnement de l'id
            $crit->setId(h($row['idCritere']));
            //ajout de l'objet à la fin du tableau
            $critList[] = $crit;
        }
        //retourne le tableau d'objets 'critere'
        return $critList;   
    }
    
    
    
    public function getListEleve(eleve $eleve)
    {
        $critList = Array();
        
        if ($eleve->getId()!=''){
            $classe= $eleve->getClasse()->getId();
            $query = "select * from `eval_classe` cl,`eval_critere` cr, eval_evaluation ev WHERE refClasse=idClasse AND archiveEvaluation=0 AND cl.refProf = ev.refProf AND refEvaluation=idEvaluation AND idClasse= ".$classe.";";
        
            //execution de la requete
            try
            {
                $result = $this->db->Query($query);
            }
            catch(PDOException $e)
            {
                die ("Erreur : ".$e->getMessage());
            }

            //Parcours du jeu d'enregistrement
            while ($row = $result->fetch())
            {
                $grilleManager = new grilleManager ($this->db);
                $grille = $grilleManager->get(h($row['refGrille']));
                $evaluationManager = new evaluationManager ($this->db);
                $evaluation = $evaluationManager->get(h($row['refEvaluation']));
                //appel du constructeur paramétré
                $crit = new critere(h($row['nomCritere']),h($row['nbPointsMaxCritere']),h($row['archiveCritere']),$grille,$evaluation);
                //positionnement de l'id
                $crit->setId(h($row['idCritere']));
                //ajout de l'objet à la fin du tableau
                $critList[] = $crit;
            }
        
        }
        //retourne le tableau d'objets 'critere'
        return $critList;  
        
    }
    
    /**
     * Sélectionne un critere dans la base.
     * 
     * Méthode de SELECT qui renvoie le critere dont l'id est spécifié en paramètre.
     * 
     * @param int ID du critere recherché
     * @return produit|boolean Renvoie l'objet critere recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `eval_critere` WHERE `idCritere`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            $grilleManager = new grilleManager ($this->db);
            $grille = $grilleManager->get(h($row['refGrille']));
            $evaluationManager = new evaluationManager ($this->db);
            $evaluation = $evaluationManager->get(h($row['refEvaluation']));
            //appel du constructeur paramétré
            $crit = new critere(h($row['nomCritere']),h($row['nbPointsMaxCritere']),h($row['archiveCritere']),$grille,$evaluation);
            //positionnement de l'id
            $crit->setId(h($row['idCritere']));

            //retourne l'objet 'critere' correpsondant
            return $crit;
        }
        else {
            return false;
        }
    }

    public function getMoyenne(critere $crit, eleve $eleve)
    {
        $query = "select AVG(nbPoints) AS avg from `eval_note` WHERE refCritere =? AND refEleve =? ";

        //Connection et execution de la requete
        try
        {
            $idCrit=$crit->getId();
            $idEleve=$eleve->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$idCrit);
            $traitement->bindparam(2,$idEleve);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la moyenne
        if ($row = $traitement->fetch())
        {
            return h($row['avg']);
        }
        return 0;
    }

    public function getNoteAttribuee(critere $crit, eleve $noteur, eleve $eleveNote)
    {
        $query = "select nbPoints from `eval_note` WHERE refCritere =? AND refEleve =? AND refNoteur =? ";

        //Connection et execution de la requete
        try
        {
            $idCrit=$crit->getId();
            $idEleveNote=$eleveNote->getId();
            $idEleveNoteur=$noteur->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$idCrit);
            $traitement->bindparam(2,$idEleveNote);
            $traitement->bindparam(3,$idEleveNoteur);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la moyenne
        if ($row = $traitement->fetch())
        {
            return h($row['nbPoints']);
        }
        return "Ø";
    }
}
