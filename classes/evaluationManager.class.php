<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les evaluations 
 .
 *
 * @author Julien
 */
class evaluationManager {
    
    private $db;
    
    /**
     * Instancie un objet évaluationManager.
     * 
     * Permet d'instanicer un objet évaluationManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie le évaluation dans la base.
     * 
     * Pour enregistrer le évaluation passé en paramètre en base de données :
     *      UPDATE si le évaluation est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param produit Eleveent à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id du évaluation ajouté ou mis à jour.
     */
    public function save(evaluation $eval)
    {        
        
        $nbRows = 0;

        // le évaluation que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($eval->getId()!=''){
            $query = "select count(*) as nb from `eval_evaluation` where `idEvaluation`=?";
            $traitement = $this->db->prepare($query);
            $param1=$eval->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si le évaluation que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `eval_evaluation` set `dateEvaluation`=?,`nomEvaluation`=?, `archiveEvaluation`=?, `refProf`=?, `refClasse`=? where `idEvaluation`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$eval->getDate();
            $traitement->bindparam(1,$param1);
            $param2=$eval->getNom();
            $traitement->bindparam(2,$param2);
            $param3=$eval->getArchive();
            $traitement->bindparam(3,$param3);
            $param4=$eval->getProf()->getId();
            $traitement->bindparam(4,$param4);
            $param5=$eval->getClasse()->getId();
            $traitement->bindparam(5,$param5);
            $param6=$eval->getId();
            $traitement->bindparam(6,$param6);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `eval_evaluation` (`dateEvaluation`,`nomEvaluation`, `archiveEvaluation`, `refProf`,`refClasse`) values (?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$eval->getDate();
            $traitement->bindparam(1,$param1);
            $param2=$eval->getNom();
            $traitement->bindparam(2,$param2);
            $param3=$eval->getArchive();
            $traitement->bindparam(3,$param3);
            $param4=$eval->getProf()->getId();
            $traitement->bindparam(4,$param4);
            $param5=$eval->getClasse()->getId();
            $traitement->bindparam(5,$param5);
            $traitement->execute();
        }
        
        if ($eval->getId() == "")
        {
            $eval->setId($this->db->lastInsertId());
        }
        return $eval->getId();
    }

    /**
     * Supprime le évaluation de la base.
     * 
     * Supprime de la base le évaluation (table "évaluation"), les commandes (table "commande") passées par celui-ci et les lignes de commandes associées (table "comporter").
     * 
     * @param produit Objet évaluation devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(evaluation $eval)
    {
        
        $nbRows = 0;

        // le evaluation que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($eval->getId()!=''){                    
            $query = "select count(*) as nb from `eval_evaluation` where `idEvaluation`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $eval->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI le évaluation que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM comporter, commande, évaluation
        //          et retourne TRUE
        if ($nbRows > 0)
        {
            $query = "DELETE FROM eval_evaluation WHERE idEvaluation=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $eval->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) évaluation(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de évaluation correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les évaluations.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) évaluation.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `eval_evaluation` ".$restriction." order by dateEvaluation DESC;";
        $evalList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            $classeManager = new classeManager ($this->db);
            $classe = $classeManager->get(h($row['refClasse']));
            $profManager = new profManager ($this->db);
            $prof = $profManager->get(h($row['refProf']));
            //appel du constructeur paramétré
            $eval = new evaluation(h($row['dateEvaluation']),h($row['nomEvaluation']),h($row['archiveEvaluation']),$prof,$classe);
            //positionnement de l'id
            $eval->setId(h($row['idEvaluation']));
            //ajout de l'objet à la fin du tableau
            $evalList[] = $eval;
        }
        //retourne le tableau d'objets 'évaluation'
        return $evalList;   
    }
    
    
    
    /**
     * Sélectionne un évaluation dans la base.
     * 
     * Méthode de SELECT qui renvoie le évaluation dont l'id est spécifié en paramètre.
     * 
     * @param int ID du évaluation recherché
     * @return produit|null Renvoie l'objet évaluation recherché ou NULL s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `eval_evaluation` WHERE `idEvaluation`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            $classeManager = new classeManager ($this->db);
            $classe = $classeManager->get(h($row['refClasse']));
            $profManager = new profManager ($this->db);
            $prof = $profManager->get(h($row['refProf']));
            //appel du constructeur paramétré
            $eval = new evaluation(h($row['dateEvaluation']),h($row['nomEvaluation']),h($row['archiveEvaluation']),$prof,$classe);
            //positionnement de l'id
            $eval->setId(h($row['idEvaluation']));

            //retourne l'objet 'évaluation' correpsondant
            return $eval;
        }
        else {
            return null;
        }
    }
  
    public function getMoyenne(evaluation $eval)
    {
        $query = "select SUM(nbPoints) AS sum from `eval_note`,`eval_critere` WHERE idCritere = refCritere AND refEvaluation=? GROUP BY refEleve;";

        //Connection et execution de la requete
        try
        {
            $id=$eval->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère les notes
        $notes=[];
        while ($row = $traitement->fetch())
        {
            $notes[]= h($row['sum']);
        }
        return count($notes)>0?array_sum($notes)/count($notes):"Ø";  //on renvoie la moyenne
    }

    public function getMax(evaluation $eval)
    {
        $query = "select SUM(nbPoints) AS sum from `eval_note`,`eval_critere` WHERE idCritere = refCritere AND refEvaluation=? GROUP BY refEleve;";

        //Connection et execution de la requete
        try
        {
            $id=$eval->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        $max="Ø";
        if($row = $traitement->fetch())
            $max= h($row['sum']);
        while ($row = $traitement->fetch())
        {
            $row['sum']>$max?$max=h($row['sum']):"Ø";
        }
        return $max; 
    }

    public function getMin(evaluation $eval)
    {
        $query = "select SUM(nbPoints) AS sum from `eval_note`,`eval_critere` WHERE idCritere = refCritere AND refEvaluation=? GROUP BY refEleve;";

        //Connection et execution de la requete
        try
        {
            $id=$eval->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        $min="Ø";
        if($row = $traitement->fetch())
            $min = h($row['sum']);
        while ($row = $traitement->fetch())
        {
            $row['sum'] < $min?$min=h($row['sum']):"Ø";
        }
        return $min; 
    }

    /**
     * renvoie le nb d'evaluations du prof en param
     * 
     * 
     * @param prof
     * @return int
     */
    public function getNbEvaluations(prof $prof)
    {
        $query = "select count(*) AS nb from `eval_evaluation` WHERE `refProf`=?;";

        //Connection et execution de la requete
        try
        {
            $id=$prof->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            return h($row['nb']);
        }
        else {
            return false;
        }
    }
}
