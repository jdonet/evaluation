<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les eleves.
 *
 * @author Ben
 */
class noteManager {
    
    private $db;
    
    /**
     * Instancie un objet eleveManager.
     * 
     * Permet d'instanicer un objet eleveManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie le eleve dans la base.
     * 
     * Pour enregistrer le eleve passé en paramètre en base de données :
     *      UPDATE si le eleve est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param produit Eleveent à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id du eleve ajouté ou mis à jour.
     */
    public function save(note $note)
    {        
        $nbRows = 0;
        
        // la note que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($note->getNoteur()!=''&&$note->getEleve()!=''&&$note->getCritere()!=''){
            $query = "select count(*) as nb from `eval_note` where `refNoteur`=? AND `refEleve`=? AND `refCritere`=?";
            $traitement = $this->db->prepare($query);
            $param1=$note->getNoteur()->getId();
            $traitement->bindparam(1,$param1);
            $param2=$note->getEleve()->getId();
            $traitement->bindparam(2,$param2);
            $param3=$note->getCritere()->getId();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si la note que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `eval_note` set `nbPoints`=? where `refNoteur`=? AND `refEleve`=? AND `refCritere`=?";
            $traitement = $this->db->prepare($query);
            $param1=$note->getPoint();
            $traitement->bindparam(1,$param1);
            $param2=$note->getNoteur()->getId();
            $traitement->bindparam(2,$param2);
            $param3=$note->getEleve()->getId();
            $traitement->bindparam(3,$param3);
            $param4=$note->getCritere()->getId();
            $traitement->bindparam(4,$param4);
            $result = $traitement->execute();
            if(!$result){
                return false;
            }else{
                return true;
            }        }
        // sinon : INSERT
        else
        {
            $query = "insert into `eval_note` (`nbPoints`, `refNoteur`,`refEleve`,`refCritere`) values (?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$note->getPoint();
            $traitement->bindparam(1,$param1);
            $param2=$note->getNoteur()->getId();
            $traitement->bindparam(2,$param2);
            $param3=$note->getEleve()->getId();
            $traitement->bindparam(3,$param3);
            $param4=$note->getCritere()->getId();
            $traitement->bindparam(4,$param4);
            $result = $traitement->execute();
            if(!$result){
                return false;
            }else{
                return true;
            }
        }

    }

    /**
     * Supprime le eleve de la base.
     * 
     * Supprime de la base le eleve (table "eleve"), les commandes (table "commande") passées par celui-ci et les lignes de commandes associées (table "comporter").
     * 
     * @param produit Objet eleve devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(note $note)
    {
        $nbRows = 0;

        // le eleve que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($note->getNoteur()!=''&&$note->getEleve()!=''&&$note->getCritere()!=''){               
            $query = "select count(*) as nb from `eval_note` where `refNoteur`=? AND `refEleve`=? AND `refCritere`=?";
            $traitement = $this->db->prepare($query);
            $param1=$note->getNoteur()->getId();
            $traitement->bindparam(1,$param1);
            $param2=$note->getEleve()->getId();
            $traitement->bindparam(2,$param2);
            $param3=$note->getCritere()->getId();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI le eleve que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM comporter, commande, eleve
        //          et retourne TRUE
        if ($nbRows > 0)
        {            
            // DELETE FROM note
            $query = "DELETE FROM eval_note WHERE `refNoteur`=? AND `refEleve`=? AND `refCritere`=?";
            $traitement = $this->db->prepare($query);
            $param1=$note->getNoteur()->getId();
            $traitement->bindparam(1,$param1);
            $param2=$note->getEleve()->getId();
            $traitement->bindparam(2,$param2);
            $param3=$note->getCritere()->getId();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }



    /**
     * Sélectionne un(des) eleve(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de eleve correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les eleves.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) eleve.
     */
    public function aNoteCritere($noteur,$eleve,$critere)
    {
    $nbRows=0;
        if ($noteur->getId()!=''&& $eleve->getId()!=''&& $critere->getId()!=''){            
        try
        {   
            $query = "select nbPoints from `eval_note` where `refNoteur`=? AND `refEleve`=? AND `refCritere`=? ";
            $traitement = $this->db->prepare($query);
            $param1=$noteur->getId();
            $traitement->bindparam(1,$param1);
            $param2=$eleve->getId();
            $traitement->bindparam(2,$param2);
            $param3=$critere->getId();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
            }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        }

        if ($nbRows > 0)
            return $nbRows;        
        else
            return 0;
    }
    
    public function aNote($noteur,$eleve,$eval)
    {
        
    $nbRows=0;
        if ($noteur->getId()!=''&& $eleve->getId()!=''){            
        try
        {   
            $query = "select count(*) as nb from `eval_note`, eval_critere WHERE refCritere=idCritere AND `refNoteur`=? AND `refEleve`=? AND refEvaluation = ? ";
            $traitement = $this->db->prepare($query);
            $param1=$noteur->getId();
            $traitement->bindparam(1,$param1);
            $param2=$eleve->getId();
            $traitement->bindparam(2,$param2);
            $param3=$eval->getId();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
            }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        }

        
        if ($nbRows > 0)
            return 1;        
        else
            return 0;
    }
    
    
    /**
     * Sélectionne un eleve dans la base.
     * 
     * Méthode de SELECT qui renvoie le eleve dont l'id est spécifié en paramètre.
     * 
     * @param int ID du eleve recherché
     * @return produit|boolean Renvoie l'objet eleve recherché ou FALSE s'il n'a pas été trouvé
     */
    public function moyenne($eleve,$critere)
    {
        $query = "select avg(nbPoints) as moyenne from `eval_note` WHERE `refEleve`=? AND `refCritere`=?";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $param1=$eleve->getId();
            $traitement->bindparam(1,$param1);
            $param2=$critere->getId();
            $traitement->bindparam(2,$param2);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            return h($row['moyenne']);
        }
        else {
            return false;
        }
    }
    /*
    public function reinitialiserNotesProf($prof)
    {
        
        if ($prof->getId()!=''){            
        try
        {   
            $query = "select * from `eval_note` n,`eval_eleve` e,`eval_classe` c,`eval_grille` where `refProf`=? AND idGrille = refGrille AND `idClasse`=classeEleve AND `refEleve`=idEleve ";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();

            //Parcours du jeu d'enregistrement
            while ($row = $traitement->fetch())
            {
                $eleveManager = new eleveManager ($this->db);
                $noteur = $eleveManager->get($row['refNoteur']);
                $eleve = $eleveManager->get($row['refEleve']);
                $critereManager = new critereManager($this->db);
                $critere = $critereManager->get($row['refCritere']);
                //appel du constructeur paramétré
                $note = new note($noteur,$eleve,$critere,$row['nbPoints']);
                $this->delete($note);
            }
            
            return true;
            }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        }
        return false;
    }
    */
    public function reinitialiserNotesProfEleve($prof,$eleve,$noteur,$eval)
    {
        
        if ($prof->getId()!=''){            
        try
        {   
            $query = "select * from `eval_note`,`eval_eleve`,`eval_classe`,`eval_critere`, eval_evaluation e WHERE idEvaluation=refEvaluation AND refCritere=idCritere AND e.refProf=? AND`idClasse`=classeEleve AND `refEleve`=idEleve AND idEleve=? AND refNoteur=? AND idEvaluation=? ";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getId();
            $traitement->bindparam(1,$param1);
            $param2=$eleve->getId();
            $traitement->bindparam(2,$param2);
            $param3=$noteur->getId();
            $traitement->bindparam(3,$param3);
            $param4=$eval->getId();
            $traitement->bindparam(4,$param4);
            $traitement->execute();

            //Parcours du jeu d'enregistrement
            while ($row = $traitement->fetch())
            {
                $eleveManager = new eleveManager ($this->db);
                $noteur = $eleveManager->get(h($row['refNoteur']));
                $eleve = $eleveManager->get(h($row['refEleve']));
                $critereManager = new critereManager($this->db);
                $critere = $critereManager->get(h($row['refCritere']));
                //appel du constructeur paramétré
                $note = new note($noteur,$eleve,$critere,h($row['nbPoints']));
                $this->delete($note);
            }
            
            return true;
            }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        }
        return false;
    }
    
    
}
