<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les classes.
 *
 * @author Ben
 */
class classeManager {
    
    private $db;
    
    /**
     * Instancie un objet classeManager.
     * 
     * Permet d'instanicer un objet classeManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    public function save(classe $class)
    {        
        $nbRows = 0;
        
        // le secteur que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($class->getId()!=''){
            $query = "select count(*) as nb from `eval_classe` where `idClasse`=?";
            $traitement = $this->db->prepare($query);
            $param1=$class->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si le secteur que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `eval_classe` set `nomClasse`=?, `refProf`=? where `idClasse`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$class->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$class->getProf()->getId();
            $traitement->bindparam(2,$param2);
            $param3=$class->getId();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            
            $query = "insert into `eval_classe` (`nomClasse`, `refProf`) values (?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$class->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$class->getProf()->getId();
            $traitement->bindparam(2,$param2);
            $traitement->execute();
        }
        
        if ($class->getId() == "")
        {
            $class->setId($this->db->lastInsertId());
        }
        return $class->getId();
    }

    /**
     * Supprime le classe de la base.
     * 
     * Supprime de la base le classe (table "classe"), les commandes (table "commande") passées par celui-ci et les lignes de commandes associées (table "comporter").
     * 
     * @param produit Objet classe devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(classe $class)
    {
        $nbRows = 0;

        // le classe que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($class->getId()!=''){                    
            $query = "select count(*) as nb from `eval_classe` where `idClasse`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $class->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI le classe que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM comporter, commande, classe
        //          et retourne TRUE
        if ($nbRows > 0)
        {   
                     
            // DELETE FROM eleve
            $query = "DELETE FROM eval_eleve WHERE classeEleve=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $class->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
                        
            // DELETE FROM classe
            $query = "DELETE FROM eval_classe WHERE idClasse=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $class->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `eval_classe` ".$restriction." order by nomClasse;";
        $classeList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            $profManager = new profManager ($this->db);
            $prof = $profManager->get(h($row['refProf']));
            //appel du constructeur paramétré
            $classe = new classe(h($row['nomClasse']),$prof);
            //positionnement de l'id
            $classe->setId(h($row['idClasse']));
            //ajout de l'objet à la fin du tableau
            $classeList[] = $classe;
        }
        //retourne le tableau d'objets 'classe'
        return $classeList;   
    }
    
    /**
     * Sélectionne un classe dans la base.
     * 
     * Méthode de SELECT qui renvoie le classe dont l'id est spécifié en paramètre.
     * 
     * @param int ID du classe recherché
     * @return produit|boolean Renvoie l'objet classe recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `eval_classe` WHERE `idClasse`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            $profManager = new profManager ($this->db);
            $prof = $profManager->get(h($row['refProf']));
            //appel du constructeur paramétré
            $classe = new classe(h($row['nomClasse']),$prof);
            //positionnement de l'id
            $classe->setId(h($row['idClasse']));

            //retourne l'objet 'classe' correpsondant
            return $classe;
        }
        else {
            return false;
        }
    }
   /**
     * renvoie le nb de classe du prof en param
     * 
     * 
     * @param prof
     * @return int
     */
    public function getNbClasses(prof $prof)
    {
        $query = "select count(*) AS nb from `eval_classe` WHERE `refProf`=?;";

        //Connection et execution de la requete
        try
        {
            $id=$prof->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            return h($row['nb']);
        }
        else {
            return false;
        }
    }
        
    
}
