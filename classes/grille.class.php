<?php
/**
 * Classe représentant les clients.
 * 
 * Les clients sont définis par :
 *     <br>- un id unique
 *     <br>- un nom
 *     <br>- un prénom
 *     <br>- une adresse (comprenant la rue, le code postal et la ville).
 *
 * @author Ben
 */
class grille{
    private $id="";
    private $nom="";
    private $active="";
    private $prof="";
 
    /**
     * Instancie un objet grille.
     */
    public function __construct($nom, $ac, $pr){
        $this->nom = $nom;
        $this->active = $ac;
        $this->prof = $pr;
    }
    
   // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setNom($n){$this->nom = $n;}
    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getActive(){return $this->active;}
    public function getProf(){return $this->prof;}
    
   
}
