<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les profs.
 *
 * @author Ben
 */
class profManager {
    
    private $db;
    
    /**
     * Instancie un objet profManager.
     * 
     * Permet d'instanicer un objet profManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie le prof dans la base.
     * 
     * Pour enregistrer le prof passé en paramètre en base de données :
     *      UPDATE si le prof est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param produit Profent à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id du prof ajouté ou mis à jour.
     */
    public function save(prof $prof)
    {        
        $nbRows = 0;

        // le secteur que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($prof->getId()!=''){
            $query = "select count(*) as nb from `eval_prof` where `idProf`=?";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si le secteur que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `eval_prof` set `nomProf`=?,`loginProf`=?, `passProf`=?, `confirmation_token`=?, `confirmed_at`=? where `idProf`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$prof->getLogin();
            $traitement->bindparam(2,$param2);
            $param3=$prof->getPass();
            $traitement->bindparam(3,$param3);
            $param4=$prof->getToken();
            $traitement->bindparam(4,$param4);
            $param5=$prof->getConfirmedAt();
            $traitement->bindparam(5,$param5);
            $param6=$prof->getId();
            $traitement->bindparam(6,$param6);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `eval_prof` (`nomProf`, `loginProf`,`passProf`, `confirmation_token`, `confirmed_at`) values (?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$prof->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$prof->getLogin();
            $traitement->bindparam(2,$param2);
            $param3=$prof->getPass();
            $traitement->bindparam(3,$param3);
            $param4=$prof->getToken();
            $traitement->bindparam(4,$param4);
            $param5=$prof->getConfirmedAt();
            $traitement->bindparam(5,$param5);
            $traitement->execute();
        }
        
        if ($prof->getId() == "") // dans le cas d'une insertion
        {
            $prof->setId($this->db->lastInsertId());
            
            //on ajoute une grille exemple
            $grilleManager = new grilleManager ($this->db);
            $grille = new grille("Ma grille 1",1,$prof);
            $grilleManager->save($grille);

            //on ajoute deux criteres exemple
            $critereManager = new critereManager ($this->db);
            $critereManager->save(new critere("Qualité de la présentation",12,0,$grille,null));
            $critereManager->save(new critere("Qualité du support",8,0,$grille,null));
        }
        return $prof->getId();
    }

    /**
     * Supprime le prof de la base.
     * 
     * Supprime de la base le prof (table "prof"), les commandes (table "commande") passées par celui-ci et les lignes de commandes associées (table "comporter").
     * 
     * @param produit Objet prof devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(prof $prof)
    {
        $nbRows = 0;

        // le prof que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($prof->getId()!=''){                    
            $query = "select count(*) as nb from `eval_prof` where `idProf`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI le prof que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM comporter, commande, prof
        //          et retourne TRUE
        if ($nbRows > 0)
        {            
            // DELETE FROM note
            $query = "DELETE FROM eval_note , eval_eleve,eval_classe WHERE refEleve=idEleve AND refClasse=idClasse AND refProf=idProf AND refProf=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
                        
            // DELETE FROM prof
            $query = "DELETE FROM eval_prof WHERE idProf=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $prof->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) prof(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de prof correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les profs.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) prof.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `eval_prof` ".$restriction." order by nomProf;";
        $profList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            //appel du constructeur paramétré
            $prof = new prof(h($row['nomProf']),h($row['loginProf']),h($row['passProf']),h($row['confirmation_token']),h($row['confirmed_at']));
            //positionnement de l'id
            $prof->setId(h($row['idProf']));
            //ajout de l'objet à la fin du tableau
            $profList[] = $prof;
        }
        //retourne le tableau d'objets 'prof'
        return $profList;   
    }
    
    /**
     * Sélectionne un prof dans la base.
     * 
     * Méthode de SELECT qui renvoie le prof dont l'id est spécifié en paramètre.
     * 
     * @param int ID du prof recherché
     * @return produit|boolean Renvoie l'objet prof recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `eval_prof` WHERE `idProf`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            //appel du constructeur paramétré
            $prof = new prof(h($row['nomProf']),h($row['loginProf']),h($row['passProf']),h($row['confirmation_token']),h($row['confirmed_at']));
            //positionnement de l'id
            $prof->setId(h($row['idProf']));
            //retourne l'objet 'prof' correpsondant
            return $prof;
        }
        else {
            return false;
        }
    }
    public function verifLogin($login)
    {
        //$query = "select * from `eval_prof` WHERE (`nomProf`=? OR `loginProf`=?) AND `confirmed_at` IS NOT NULL;";
        $query = "select * from `eval_prof` WHERE `loginProf`=? AND `confirmed_at` IS NOT NULL;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$login);
//            $traitement->bindparam(2,$login);            
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            //appel du constructeur paramétré
            $prof = new prof(h($row['nomProf']),h($row['loginProf']),h($row['passProf']),h($row['confirmation_token']),h($row['confirmed_at']));
            //positionnement de l'id
            $prof->setId(h($row['idProf']));
            //retourne l'objet 'prof' correpsondant
            return $prof;
        }
        else {
            return false;
        }
    }
    /*
    public function existeLogin($login)
    {
        $query = "select * from `eval_prof` WHERE `nomProf`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$login);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            
            //retourne true
            return true;
        }
        else {
            return false;
        }
    }*/
    public function existeMail($mail)
    {
        $query = "select * from `eval_prof` WHERE `loginProf`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$mail);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            
            //retourne true
            return true;
        }
        else {
            return false;
        }
    }
        
    
}
