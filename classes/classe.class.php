<?php
/**
 * Classe représentant les clients.
 * 
 * Les clients sont définis par :
 *     <br>- un id unique
 *     <br>- un nom
 *     <br>- un prénom
 *     <br>- une adresse (comprenant la rue, le code postal et la ville).
 *
 * @author Ben
 */
class classe{
    private $id="";
    private $nom="";
    private $prof="";
    
    /**
     * Instancie un objet client.
     *  
     * @param string Nom du client.
     * @param string Prénom du client.
     * @param string Rue du client.
     * @param string Code postal du client.
     * @param string Ville du client.
     * 
     */
    public function __construct($nom,$prof){
        $this->nom = $nom;
        $this->prof = $prof;
    }
    
    // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setNom($n){$this->nom = $n;}

    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getProf(){return $this->prof;}
    
   
}
