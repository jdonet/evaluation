<?php
/**
 * Classe représentant les clients.
 * 
 * Les clients sont définis par :
 *     <br>- un id unique
 *     <br>- un nom
 *     <br>- un prénom
 *     <br>- une adresse (comprenant la rue, le code postal et la ville).
 *
 * @author Ben
 */
class note{
    private $noteur="";
    private $eleve="";
    private $critere="";
    private $point="";
 
    /**
     * Instancie un objet client.
     *  
     * @param string Nom du client.
     * @param string Prénom du client.
     * @param string Rue du client.
     * @param string Code postal du client.
     * @param string Ville du client.
     * 
     */
    public function __construct($noteur, $eleve, $critere, $point){
        $this->noteur = $noteur;
        $this->eleve = $eleve;
        $this->critere = $critere;
        $this->point = $point;
    }
    
   
    
    // Accesseurs chargés d'exposer les attributs
    public function getNoteur(){return $this->noteur;}
    public function getEleve(){return $this->eleve;}
    public function getCritere(){return $this->critere;}
    public function getPoint(){return $this->point;}
    
   
}
