<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les authentifications.
 *
 * @author Julien
 */
class loginManager {
    
    private $db;
    
    /**
     * Instancie un objet adherentManager.
     * 
     * Permet d'instancier un objet adherentManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    
    
    /**
     * Vérifie l'authentification dans la base.
     * 
     * Méthode qui vérifie renvoie un token si authentification ok.
     * 
     * @param string login recherché
     * @param string pass recherché
     * @return string|boolean Renvoie le token ou FALSE si erreur de connexion
     */
    public function checkLogin($login,$pass)
    {
        $query = "select * from `eval_eleve` WHERE `loginEleve`=? AND `passEleve`=?;";
        $tabReturned=array();
        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$login);
            $traitement->bindparam(2,$pass); //pwd en clair ici ...
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }
        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            //on vérifie que le compte est encore valide
            //if ((int)$row['activeEleve']===1){
                $token = md5(uniqid($login, true));
                $query = "insert into `eval_token` (`token`, `token_generated`,`token_validity`,`refEleveMobile`) values (?,?,?,?);";
                $traitement = $this->db->prepare($query);
                $param1=$token;
                $traitement->bindparam(1,$param1);
                $param2=date("Y-m-d H:i:s");
                $traitement->bindparam(2,$param2);
                $param3=date("Y-m-d H:i:s",mktime(date("H") +1, date("i"), date("s"), date("m")  , date("d"), date("Y"))); //dans une heure
                $traitement->bindparam(3,$param3);
                $param4=h($row["idEleve"]);
                $traitement->bindparam(4,$param4);
                $traitement->execute();
                $tabReturned["token"]=$token;
                $tabReturned["id"]=h($row["idEleve"]);
                $tabReturned["nom"]=h($row["prenomEleve"])." ".h($row["nomEleve"]);
                return $tabReturned;
            /*}else{
                return false;    
            }*/
        }
        else {
            return false;
        }
    }
    /**
     * Génère seulement un token pour un user pour les stats
     */
    public function getToken($idProf,$idEleve)
    {
        $token = md5(uniqid($idProf*rand(0,1000), true));
        $query = "insert into `eval_token` (`token`, `token_generated`,`token_validity`,`refProf`,`refEleve`) values (?,?,?,?,?);";
        $traitement = $this->db->prepare($query);
        $param1=$token;
        $traitement->bindparam(1,$param1);
        $param2=date("Y-m-d H:i:s");
        $traitement->bindparam(2,$param2);
        $param3=date("Y-m-d H:i:s",mktime(date("H") +1, date("i"), date("s"), date("m")  , date("d"), date("Y"))); //dans une heure
        $traitement->bindparam(3,$param3);
        $param4=$idProf;
        $traitement->bindparam(4,$param4);
        $param5=$idEleve;
        $traitement->bindparam(5,$param5);
        $traitement->execute();
        return $token;
    }
    /**
     * Vérifie la validité du token dans la base.
     * 
     * @param string token recherché
     * @return int | boolean Renvoie l'id de l'élève ou FALSE si erreur de connexion
     */
    public function checkToken($token)
    {
        $query = "select * from eval_token WHERE `token`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$token);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            //on vérifie que le token est encore valide
            if (strtotime(h($row['token_validity'])) >= strtotime(date("Y-m-d H:i:s"))){
                return h($row['refEleveMobile']);
            }else{
                return false;    
            }
        }
        else {
            return false;
        }
    }
    /**
     * Renvoie le nb cnx pour le prof connecté
     * 
     * @param prof : prof
     * @return int
     */
    public function getNbCnx(prof $prof)
    {
        $query = "select count(*) as nb from `eval_token` WHERE `refProf`=?;";

        //Connection et execution de la requete
        try
        {
            $id=$prof->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            return h($row["nb"]);
        }
        else {
            return false;
        }
    }
    /**
     * Renvoie la date de la dernière cnx pour le prof connecté
     * 
     * @param prof : prof
     * @return int
     */
    public function getLastCnx(prof $prof)
    {
        $query = "select max(token_generated) as max from `eval_token` WHERE `refProf`=?;";

        //Connection et execution de la requete
        try
        {
            $id=$prof->getId();
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            return h($row["max"]);
        }
        else {
            return false;
        }
    }
}