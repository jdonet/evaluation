<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les eleves.
 *
 * @author Ben
 */
class eleveManager {
    
    private $db;
    
    /**
     * Instancie un objet eleveManager.
     * 
     * Permet d'instanicer un objet eleveManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie le eleve dans la base.
     * 
     * Pour enregistrer le eleve passé en paramètre en base de données :
     *      UPDATE si le eleve est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param produit Eleveent à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id du eleve ajouté ou mis à jour.
     */
    public function save(eleve $elev)
    {        
        $nbRows = 0;
        
        // l eleve que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($elev->getId()!=''){
            $query = "select count(*) as nb from `eval_eleve` where `idEleve`=?";
            $traitement = $this->db->prepare($query);
            $param1=$elev->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si l eleve que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `eval_eleve` set `nomEleve`=?, `prenomEleve`=?, `actifEleve`=?, `loginEleve`=?, `passEleve`=?, `classeEleve`=? where `idEleve`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$elev->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$elev->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$elev->getActif();
            $traitement->bindparam(3,$param3);
            $param4=$elev->getLogin();
            $traitement->bindparam(4,$param4);
            $param5=$elev->getPass();
            $traitement->bindparam(5,$param5);
            $param6=$elev->getClasse()->getId();
            $traitement->bindparam(6,$param6);
            $param7=$elev->getId();
            $traitement->bindparam(7,$param7);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `eval_eleve` (`nomEleve`, `prenomEleve`,`actifEleve`,`loginEleve`,`passEleve`,`classeEleve`) values (?,?,?,?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$elev->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$elev->getPrenom();
            $traitement->bindparam(2,$param2);
            $param3=$elev->getActif();
            $traitement->bindparam(3,$param3);
            $param4=$elev->getLogin();
            $traitement->bindparam(4,$param4);
            $param5=$elev->getPass();
            $traitement->bindparam(5,$param5);
            $param6=$elev->getClasse()->getId();
            $traitement->bindparam(6,$param6);
            $traitement->execute();
        }
        
        if ($elev->getId() == "")
        {
            $elev->setId($this->db->lastInsertId());
        }
        return $elev->getId();
    }

    /**
     * Supprime le eleve de la base.
     * 
     * Supprime de la base le eleve (table "eleve"), les commandes (table "commande") passées par celui-ci et les lignes de commandes associées (table "comporter").
     * 
     * @param produit Objet eleve devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(eleve $elev)
    {
        $nbRows = 0;

        // le eleve que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($elev->getId()!=''){                    
            $query = "select count(*) as nb from `eval_eleve` where `idEleve`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $elev->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI le eleve que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM comporter, commande, eleve
        //          et retourne TRUE
        if ($nbRows > 0)
        {            
            // DELETE FROM note
            $query = "DELETE FROM eval_note WHERE refEleve=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $elev->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
                        
            // DELETE FROM eleve
            $query = "DELETE FROM eval_eleve WHERE idEleve=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $elev->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) eleve(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de eleve correspondant aux critères de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir tous les eleves.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) eleve.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `eval_eleve` ".$restriction." order by nomEleve,prenomEleve;";
        $elevList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            $classeManager = new classeManager ($this->db);
            $classe = $classeManager->get(h($row['classeEleve']));
            //appel du constructeur paramétré
            $elev = new eleve(h($row['nomEleve']),h($row['prenomEleve']),h($row['actifEleve']),h($row['loginEleve']),h($row['passEleve']),$classe);
            //positionnement de l'id
            $elev->setId(h($row['idEleve']));
            //ajout de l'objet à la fin du tableau
            $elevList[] = $elev;
        }
        //retourne le tableau d'objets 'eleve'
        return $elevList;   
    }
    
    
    /**
     * Sélectionne un eleve dans la base.
     * 
     * Méthode de SELECT qui renvoie le eleve dont l'id est spécifié en paramètre.
     * 
     * @param int ID du eleve recherché
     * @return produit|boolean Renvoie l'objet eleve recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `eval_eleve` WHERE `idEleve`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            $classeManager = new classeManager ($this->db);
            $classe = $classeManager->get(h($row['classeEleve']));
            //appel du constructeur paramétré
            $elev = new eleve(h($row['nomEleve']),h($row['prenomEleve']),h($row['actifEleve']),h($row['loginEleve']),h($row['passEleve']),$classe);
            //positionnement de l'id
            $elev->setId(h($row['idEleve']));
            //retourne l'objet 'eleve' correpsondant
            return $elev;
        }
        else {
            return false;
        }
    }
    /**
     * Activer un eleve dans la base.
     * 
     * Méthode de UPDATE 
     * 
     * @param int ID du eleve recherché
     * @return produit|boolean Renvoie l'objet eleve recherché ou FALSE s'il n'a pas été trouvé
     */
    public function activer(eleve $elev)
    {        
        $nbRows = 0;

        // je désactive les autres de la classe
        if ($elev->getId()!=''){
            $query = "UPDATE `eval_eleve`  set actifEleve=0 where `classeEleve`=?";
            $traitement = $this->db->prepare($query);
            $param1=$elev->getClasse()->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
           
            //j'active celui qui m'interesse
            $query = "UPDATE `eval_eleve`  set actifEleve=1 where `idEleve`=?";
            $traitement = $this->db->prepare($query);
            $param1=$elev->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            }
        }
        
        /**
         * Renvoie l'élève actif dans la classe passée en paramètre
         */
        public function actif(classe $classe)
        {        
        $nbRows = 0;
        
            $query = "SELECT *  from `eval_eleve` where actifEleve=1 AND `classeEleve`=?";
            $traitement = $this->db->prepare($query);
            $param1=$classe->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            
            
         //On récupère la première et seule ligne du jeu d'enregistrement	
           if($row = $traitement->fetch()) {
               //retourne l'objet 'eleve' correpsondant
               return $this->get(h($row['idEleve']));

           }
           else {
               $el= new eleve("", "", "", "", "","");
               $el->setId("-1");
               return $el;
           }
            
        }

        /**
         * Check if login exist
         */
        public function existeLogin($login)
        {
            $query = "select * from `eval_eleve` WHERE `loginEleve`=? ;";
            //$query = "select * from `eval_eleve` WHERE `loginEleve`=? AND idEleve!=?;";
            
            //Connection et execution de la requete
            try
            {
                $traitement = $this->db->prepare($query);
                $traitement->bindparam(1,$login);
                //$traitement->bindparam(2,$idEleve);
                $traitement->execute();
            }
            catch(PDOException $e)
            {
                die ("Erreur : ".$e->getMessage());
            }

            //On récupère la première et seule ligne du jeu d'enregistrement	
            if($row = $traitement->fetch()) {
                
                //retourne true
                return true;
            }
            else {
                return false;
            }
        }
        /**
         * Get id from login
         */
        public function getIdFromLogin($login)
        {
            $query = "select * from `eval_eleve` WHERE `loginEleve`=? ;";
            //$query = "select * from `eval_eleve` WHERE `loginEleve`=? AND idEleve!=?;";
            
            //Connection et execution de la requete
            try
            {
                $traitement = $this->db->prepare($query);
                $traitement->bindparam(1,$login);
                //$traitement->bindparam(2,$idEleve);
                $traitement->execute();
            }
            catch(PDOException $e)
            {
                die ("Erreur : ".$e->getMessage());
            }

            //On récupère la première et seule ligne du jeu d'enregistrement	
            if($row = $traitement->fetch()) {
                
                //retourne true
                return h($row['idEleve']);
            }
            else {
                return false;
            }
        }
        
    
}
