<?php
require_once ("database.class.php");

/**
 * Classe d'accès aux données concernant les grilles 
 .
 *
 * @author Julien
 */
class grilleManager {
    
    private $db;
    
    /**
     * Instancie un objet eleveManager.
     * 
     * Permet d'instanicer un objet eleveManager qui nous permettra ensuite d'accéder aux données de la base spécifiée en paramètre.
     *  
     * @param database Instance de la classe database.
     */
    public function __construct($database)
    {
        //Dès le constructeur du manager on récupère la connection
        // à la base de données défini dans la classe database
        $this->db=$database;
    }    
    
    /**
     * Enregistre ou Modifie une grille dans la base.
     * 
     * Pour enregistrer une grille passée en paramètre en base de données :
     *      UPDATE si la grille est déjà existant;
     *      INSERT sinon (si id non trouvé ou non spécifié).
     * 
     * @param grille Eleveent à enregister ou mettre à jour.
     * 
     * @return int Retourne l'id de la grille ajoutée ou mise à jour.
     */
    public function save(grille $grille)
    {        
        
        $nbRows = 0;

        // la grille que nous essayons de sauvegarder existe-t-elle dans la  base de données ?
        if ($grille->getId()!=''){
            $query = "select count(*) as nb from `eval_grille` where `idGrille`=?";
            $traitement = $this->db->prepare($query);
            $param1=$grille->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }
        
        // Si la grille que nous essayons de sauvegarder existe dans la base de données : UPDATE
        if ($nbRows > 0)
        {
            $query = "update `eval_grille` set `nomGrille`=?,  `activeGrille`=?, `refProf`=? where `idGrille`=?;";
            $traitement = $this->db->prepare($query);
            $param1=$grille->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$grille->getActive();
            $traitement->bindparam(2,$param2);
            $param3=$grille->getProf()->getId();
            $traitement->bindparam(3,$param3);
            $param4=$grille->getId();
            $traitement->bindparam(4,$param4);
            $traitement->execute();
        }
        // sinon : INSERT
        else
        {
            $query = "insert into `eval_grille` (`nomGrille`, `activeGrille`,`refProf`) values (?,?,?);";
            $traitement = $this->db->prepare($query);
            $param1=$grille->getNom();
            $traitement->bindparam(1,$param1);
            $param2=$grille->getActive();
            $traitement->bindparam(2,$param2);
            $param3=$grille->getProf()->getId();
            $traitement->bindparam(3,$param3);
            $traitement->execute();
        }
        
        if ($grille->getId() == "")
        {
            $grille->setId($this->db->lastInsertId());
        }
        return $grille->getId();
    }

    /**
     * Supprime la grille de la base.
     * 
     * Supprime de la base la grille
     * 
     * @param grille Objet grille devant être supprimé.
     * @return boolean Retourne TRUE si la suppression est un succès, FALSE sinon.
     */    
    public function delete(grille $grille)
    {
        
        $nbRows = 0;

        // lae grille que nous essayons de supprimer existe-t-il dans la  base de données ?
        if ($grille->getId()!=''){                    
            $query = "select count(*) as nb from `eval_grille` where `idGrille`=?";
            $traitement = $this->db->prepare($query);
            $param1 = $grille->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
            $ligne = $traitement->fetch();
            $nbRows=$ligne[0];
        }

        // SI la grille que nous essayons de supprimer existe dans bd
        // ALORS
        //      DELETE FROM grille
        //          et retourne TRUE
        if ($nbRows > 0)
        {
            //suppression des crières
            $query = "DELETE FROM eval_critere WHERE refGrille=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $grille->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();            
            

            $query = "DELETE FROM eval_grille WHERE idGrille=?;";
            $traitement = $this->db->prepare($query);
            $param1 = $grille->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();            
            return true;
        }
        // SINON
        //      retourne FALSE
        else {
            return false;
        }
    }

    /**
     * Sélectionne un(des) grille(s) dans la base.
     * 
     * Méthode générique de SELECT qui renvoie un tableau de grilles correspondant aux grilles de sélection spécifiés.
     * Si aucun paramètre n'est précisé, la valeur par défaut du paramètre 'WHERE 1' permet d'obtenir toutes les grilles.
     * 
     * @param string Chaîne de caractère devant être une restriction SQL valide.
     * @return array Renvoie un tableau d'objet(s) grille.
     */
    public function getList($restriction='WHERE 1')
    {
        $query = "select * from `eval_grille` ".$restriction." order by nomGrille;";
        $grilleList = Array();

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            $profManager = new profManager ($this->db);
            $prof = $profManager->get(h($row['refProf']));
            //appel du constructeur paramétré
            $grille = new grille(h($row['nomGrille']),h($row['activeGrille']),$prof);
            //positionnement de l'id
            $grille->setId(h($row['idGrille']));
            //ajout de l'objet à la fin du tableau
            $grilleList[] = $grille;
        }
        //retourne le tableau d'objets 'grille'
        return $grilleList;   
    }
    
    
    
    /**
     * Sélectionne une grille dans la base.
     * 
     * Méthode de SELECT qui renvoie la grille dont l'id est spécifié en paramètre.
     * 
     * @param int ID de la grille recherchée
     * @return produit|boolean Renvoie l'objet grille recherché ou FALSE s'il n'a pas été trouvé
     */
    public function get($id)
    {
        $query = "select * from `eval_grille` WHERE `idGrille`=?;";

        //Connection et execution de la requete
        try
        {
            $traitement = $this->db->prepare($query);
            $traitement->bindparam(1,$id);
            $traitement->execute();
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        //On récupère la première et seule ligne du jeu d'enregistrement	
        if($row = $traitement->fetch()) {
            $profManager = new profManager ($this->db);
            $prof = $profManager->get(h($row['refProf']));
            //appel du constructeur paramétré
            $grille = new grille(h($row['nomGrille']),h($row['activeGrille']),$prof);
            //positionnement de l'id
            $grille->setId(h($row['idGrille']));

            //retourne l'objet 'eleve' correpsondant
            return $grille;
        }
        else {
            return false;
        }
    }

    public function afficher4PremiersCriteres(grille $grille)
    {
        $query = "select * from `eval_critere` WHERE refGrille= ".$grille->getId()." LIMIT 4;";

        //execution de la requete
        try
        {
            $result = $this->db->Query($query);
        }
        catch(PDOException $e)
        {
            die ("Erreur : ".$e->getMessage());
        }

        $toString="<table border='1' width='100%' height='70%'>";
        //Parcours du jeu d'enregistrement
        while ($row = $result->fetch())
        {
            $toString .= "<tr><td>".h($row['nomCritere']) . "</td><td>" . h($row['nbPointsMaxCritere']) . " points </td></tr>";
            
        }
        $toString.="</table>";
        //retourne le tableau d'objets 'grille'
        return $toString;   
    }


    /**
     * Activer une grille dans la base.
     * 
     * Méthode de UPDATE 
     * 
     * @param int ID de la grille recherchée
     * @return produit|boolean Renvoie l'objet grille recherchée ou FALSE s'il n'a pas été trouvé
     */
    public function activer(grille $grille)
    {        
        $nbRows = 0;
        // le secteur que nous essayons de sauvegarder existe-t-il dans la  base de données ?
        if ($grille->getId()!=''){
            $query = "UPDATE `eval_grille`  set activeGrille=0 where `refProf`=?";
            $traitement = $this->db->prepare($query);
            $param1=$grille->getProf()->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();
           
            
            $query = "UPDATE `eval_grille`  set activeGrille=1 where `idGrille`=?";
            $traitement = $this->db->prepare($query);
            $param1=$grille->getId();
            $traitement->bindparam(1,$param1);
            $traitement->execute();    
        }
    }
    
}
