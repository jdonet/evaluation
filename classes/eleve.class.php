<?php
/**
 * Classe représentant les clients.
 * 
 * Les clients sont définis par :
 *     <br>- un id unique
 *     <br>- un nom
 *     <br>- un prénom
 *     <br>- une adresse (comprenant la rue, le code postal et la ville).
 *
 * @author Ben
 */
class eleve implements JsonSerializable {
    private $id="";
    private $nom="";
    private $prenom="";
    private $actif="";
    private $login="";
    private $pass="";
    private $classe="";
 
    /**
     * Instancie un objet client.
     *  
     * @param string Nom du client.
     * @param string Prénom du client.
     * @param string Rue du client.
     * @param string Code postal du client.
     * @param string Ville du client.
     * 
     */
    public function __construct($nom, $pre, $ac, $log, $pwd,$clas){
        $this->nom = $nom;
        $this->prenom = $pre;
        $this->actif = $ac;
        $this->login = $log;
        $this->pass = $pwd;
        $this->classe = $clas;
    }
    
    // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setNom($n){$this->nom = $n;}
    public function setPrenom($p){$this->prenom = $p;}
    public function setActif($a){$this->actif = $a;}
    public function setClasse($c){$this->classe = $c;}

    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getPrenom(){return $this->prenom;}
    public function getActif(){return $this->actif;}
    public function getLogin(){return $this->login;}
    public function getPass(){return $this->pass;}
    public function getClasse(){return $this->classe;}
    
   /**
     * Spécifie les données qui doivent être linéarisées en JSON.
     * 
     * Linéarise l'objet en une valeur qui peut être linéarisé nativement par la fonction json_encode().
     * 
     * @return mixed Retourne les données qui peuvent être linéarisées par la fonction json_encode()
     */
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'prenom' => $this->prenom
        ];
    }
}
