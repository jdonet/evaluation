<?php
/**
 * Classe représentant les critères.
 * 
 * @author Julien
 */
class critere implements JsonSerializable {
    private $id="";
    private $nom="";
    private $pointMax="";
    private $archive="";
    private $grille="";
    private $evaluation="";

    public function __construct($nom, $pts, $ar, $grille,$e){
        $this->nom = $nom;
        $this->pointMax = $pts;
        $this->archive = $ar;
        $this->grille = $grille;
        $this->evaluation = $e;
    }
    
   // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setNom($n){$this->nom = $n;}
    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getPointMax(){return $this->pointMax;}
    public function getArchive(){return $this->archive;}
    public function getGrille(){return $this->grille;}
    public function getEvaluation(){return $this->evaluation;}
    
 /**
     * Spécifie les données qui doivent être linéarisées en JSON.
     * 
     * Linéarise l'objet en une valeur qui peut être linéarisé nativement par la fonction json_encode().
     * 
     * @return mixed Retourne les données qui peuvent être linéarisées par la fonction json_encode()
     */
    public function jsonSerialize() {
        return [
            'id' => $this->id,
            'nom' => $this->nom,
            'pointMax' => $this->pointMax
        ];
    }  
}
