<?php
/**
 * Classe représentant les evaluations.
 * 
 * @author Julien
 */
class evaluation{
    private $id="";
    private $date="";
    private $nom="";
    private $archive="";
    private $prof="";
    private $classe="";
    
    public function __construct($date, $nom, $archive, $prof, $classe){
        $this->date = $date;
        $this->nom = $nom;
        $this->archive = $archive;
        $this->prof = $prof;
        $this->classe = $classe;
    }
    
   // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setDate($d){$this->date = $d;}
    public function setNom($n){$this->nom = $n;}
    public function setArchive($a){$this->archive = $a;}
    public function setProf($p){$this->prof = $p;}
    public function setClasse($c){$this->classe = $c;}
    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getDate(){return $this->date;}
    public function getNom(){return $this->nom;}
    public function getArchive(){return $this->archive;}
    public function getProf(){return $this->prof;}
    public function getClasse(){return $this->classe;}
    
   
}
