<?php

class prof{
    private $id="";
    private $nom="";
    private $login="";
    private $pass="";
    private $token="";
    private $confirmedAt="";
    
    
    public function __construct($nom, $log, $pwd, $tkn = null, $ca=null){
        $this->nom = $nom;
        $this->login = $log;
        $this->pass = $pwd;
        $this->token = $tkn;
        $this->confirmedAt= $ca;
        
        
    }
    
    // Mutateurs chargés de modifier les attributs
    public function setId($id){$this->id = $id;}    
    public function setNom($n){$this->nom = $n;}
    public function setLogin($l){$this->login = $l;}
    public function setPass($p){$this->pass = $p;}
    public function setToken($t){$this->token = $t;}
    public function setConfirmedAt($ca){$this->confirmedAt = $ca;}
    
    
    // Accesseurs chargés d'exposer les attributs
    public function getId(){return $this->id;}
    public function getNom(){return $this->nom;}
    public function getLogin(){return $this->login;}
    public function getPass(){return $this->pass;}
    public function getToken(){return $this->token;}
    public function getConfirmedAt(){return $this->confirmedAt;}
   
}
