<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once ("../prof/inc/functions.php");
require_once ("../classes/database.class.php");

require_once ("../classes/eleve.class.php");
require_once ("../classes/eleveManager.class.php");
require_once ("../classes/classe.class.php");
require_once ("../classes/classeManager.class.php");
require_once ("../classes/prof.class.php");
require_once ("../classes/profManager.class.php");
require_once ("../classes/critere.class.php");
require_once ("../classes/critereManager.class.php");
require_once ("../classes/grille.class.php");
require_once ("../classes/grilleManager.class.php");
require_once ("../classes/evaluation.class.php");
require_once ("../classes/evaluationManager.class.php");
require_once ("../classes/note.class.php");
require_once ("../classes/noteManager.class.php");

require_once ("../classes/loginManager.class.php");

// Fonction de connexion
function login($login,$pass){
    $manager = new loginManager(database::getDB());
    $tabReturned = $manager->checkLogin($login,$pass);
    if(!$tabReturned['token']) //si le token est false
        echo '{"status":"error","token":"null"}';
    else
        echo '{"status":"logged","token":"'.$tabReturned['token'].'","id":"'.$tabReturned['id'].'","nom":"'.$tabReturned['nom'].'"}';
}

function checkToken($token){
    $manager = new loginManager(database::getDB());
    return $manager->checkToken($token);
}


// Retourne la liste des critères pour cet élève
function getCriteres($idEleve){
    
    $manager = new eleveManager(database::getDB());
    //recupération de la classe de l'élève
    $eleve = $manager->get($idEleve);
    //récupérer la liste des critères pour l'interro active de la classe de l'eleve
    $manager = new critereManager(database::getDB());
    $tabCriteres = $manager->getListEleve($eleve);
    echo '{"criteres":'.json_encode($tabCriteres).'}';
    
}

// Retourne l'eleve actif pour la classe de cet élève
function getEleveActif($idEleve){
    $manager = new eleveManager(database::getDB());
    //recupération de la classe de l'élève
    $eleve = $manager->get($idEleve);
    //récupérer l'eleve actif
    $eleveActif = $manager->actif($eleve->getClasse());
    echo json_encode($eleveActif);
    
}

function updateNote(int $idNoteur,int $idEleveActif,int $idCritere, int $points){
    $managerEleve = new eleveManager(database::getDB());
    $managerCritere = new critereManager(database::getDB());
    $managerNote = new noteManager(database::getDB());
    $noteur = $managerEleve->get($idNoteur);
    $eleveActif = $managerEleve->get($idEleveActif);
    $critere = $managerCritere->get($idCritere);
    $note = new note($noteur, $eleveActif, $critere, $points);
    $result = $managerNote->save($note); //nb, si la note existe déjà, elle est pas réinsérée grace au jeux des clefs étrangères?
    if($result)
        echo '{"status":"ok"}';
    else
        echo '{"status":"error"}';
}


