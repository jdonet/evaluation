<?php
require_once("facade.php");

if ($_SERVER["REQUEST_METHOD"]=="POST"){
    $PARRAY = $_POST;
}
else {
    $PARRAY = $_GET;
}

/*selection de l'action */
if (isset($PARRAY["action"]))
{
    $act=$PARRAY["action"];
    
        if($act=="login"){ //////////////////Il serait peut être bien dans la méthode de supprimer les token expirés ?
                login($PARRAY["login"],$PARRAY["pass"]);
        }elseif(isset($PARRAY["token"]) && checkToken($PARRAY["token"])!=false){ //require valid token
                //vérifier si le token correspond à l'id élève
                $manager = new loginManager(database::getDB());
                $idEleveBD= $manager->checkToken($PARRAY["token"]);
                if(isset($PARRAY["id"]) && $idEleveBD==$PARRAY["id"]){
                        if($act=="getEleveActif"){
                                getEleveActif($PARRAY["id"]);                        
                        }else if($act=="getCriteres"){
                                getCriteres($PARRAY["id"]);
                        }else if($act=="updateNote"){
                                updateNote($PARRAY["id"],$PARRAY["eleve"],$PARRAY["critere"],$PARRAY["points"]);
                        }else{
                                echo ('{"status":"error","msg":"Action inconnue"}');
                        }
                }else{
                        echo ('{"status":"error","msg":"Le token ne correspond pas à l\'utilisateur prévu"}');
                }
        }else{
                echo ('{"status":"error","msg":"Token invalide"}');
        }
        		
}
else{
        echo ('{"status":"error","msg":"Erreur, vérifiez les paramètres de votre requête http !"}');
}


