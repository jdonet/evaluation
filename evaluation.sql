-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 06 Décembre 2018 à 15:34
-- Version du serveur :  5.7.24-0ubuntu0.16.04.1
-- Version de PHP :  7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `evaluation`
--

-- --------------------------------------------------------

--
-- Structure de la table `eval_classe`
--

CREATE TABLE `eval_classe` (
  `idClasse` int(11) NOT NULL,
  `nomClasse` varchar(100) NOT NULL,
  `actifClasse` tinyint(4) NOT NULL,
  `refProf` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `eval_classe`
--

INSERT INTO `eval_classe` (`idClasse`, `nomClasse`, `actifClasse`, `refProf`) VALUES
(37, 'jdonet_classe1', 1, 33);

-- --------------------------------------------------------

--
-- Structure de la table `eval_critere`
--

CREATE TABLE `eval_critere` (
  `idCritere` int(11) NOT NULL,
  `nomCritere` varchar(200) NOT NULL,
  `nbPointsMaxCritere` tinyint(4) NOT NULL,
  `actifCritere` tinyint(1) NOT NULL,
  `refGrille` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `eval_critere`
--

INSERT INTO `eval_critere` (`idCritere`, `nomCritere`, `nbPointsMaxCritere`, `actifCritere`, `refGrille`) VALUES
(109, 'Qualité de la présentation', 14, 1, 1),
(110, 'Qualité du support', 6, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `eval_eleve`
--

CREATE TABLE `eval_eleve` (
  `idEleve` int(11) NOT NULL,
  `nomEleve` varchar(100) NOT NULL,
  `prenomEleve` varchar(100) NOT NULL,
  `actifEleve` tinyint(1) NOT NULL DEFAULT '0',
  `loginEleve` varchar(100) NOT NULL,
  `passEleve` text NOT NULL,
  `classeEleve` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `eval_eleve`
--

INSERT INTO `eval_eleve` (`idEleve`, `nomEleve`, `prenomEleve`, `actifEleve`, `loginEleve`, `passEleve`, `classeEleve`) VALUES
(333, 'jdonet_eleve1', 'e1', 1, 'jdonet_eleve1', 'eleve1', 37),
(334, 'jdonet_eleve2', 'e2', 0, 'jdonet_eleve2', 'eleve2', 37);

-- --------------------------------------------------------

--
-- Structure de la table `eval_grille`
--

CREATE TABLE `eval_grille` (
  `idGrille` int(11) NOT NULL,
  `nomGrille` varchar(255) NOT NULL,
  `activeGrille` tinyint(1) NOT NULL DEFAULT '0',
  `refProf` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `eval_grille`
--

INSERT INTO `eval_grille` (`idGrille`, `nomGrille`, `activeGrille`, `refProf`) VALUES
(1, 'Ma grille 1', 1, 33),
(3, 'tesrtGrille', 1, 33);

-- --------------------------------------------------------

--
-- Structure de la table `eval_note`
--

CREATE TABLE `eval_note` (
  `refNoteur` int(11) NOT NULL,
  `refEleve` int(11) NOT NULL,
  `refCritere` int(11) NOT NULL,
  `nbPoints` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `eval_prof`
--

CREATE TABLE `eval_prof` (
  `idProf` int(11) NOT NULL,
  `nomProf` varchar(200) NOT NULL,
  `loginProf` varchar(100) NOT NULL,
  `passProf` varchar(100) NOT NULL,
  `confirmation_token` varchar(60) DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `eval_prof`
--

INSERT INTO `eval_prof` (`idProf`, `nomProf`, `loginProf`, `passProf`, `confirmation_token`, `confirmed_at`) VALUES
(33, 'jdonet', 'jul_donet@yahoo.fr', '$2y$10$ACMS9fll.y/IubFR1wMfkePjvIzCU3wojMlES0fneIZsYOS5pWMNC', '', '2018-12-05 11:00:00');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `eval_classe`
--
ALTER TABLE `eval_classe`
  ADD PRIMARY KEY (`idClasse`),
  ADD KEY `refProf` (`refProf`);

--
-- Index pour la table `eval_critere`
--
ALTER TABLE `eval_critere`
  ADD PRIMARY KEY (`idCritere`),
  ADD KEY `refGrille` (`refGrille`);

--
-- Index pour la table `eval_eleve`
--
ALTER TABLE `eval_eleve`
  ADD PRIMARY KEY (`idEleve`),
  ADD UNIQUE KEY `loginEleve` (`loginEleve`),
  ADD KEY `classeEleve` (`classeEleve`);

--
-- Index pour la table `eval_grille`
--
ALTER TABLE `eval_grille`
  ADD PRIMARY KEY (`idGrille`),
  ADD KEY `refProf` (`refProf`);

--
-- Index pour la table `eval_note`
--
ALTER TABLE `eval_note`
  ADD KEY `refNoteur` (`refNoteur`),
  ADD KEY `refEleve` (`refEleve`),
  ADD KEY `refCritere` (`refCritere`);

--
-- Index pour la table `eval_prof`
--
ALTER TABLE `eval_prof`
  ADD PRIMARY KEY (`idProf`),
  ADD UNIQUE KEY `loginProf` (`loginProf`),
  ADD UNIQUE KEY `nomProf` (`nomProf`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `eval_classe`
--
ALTER TABLE `eval_classe`
  MODIFY `idClasse` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `eval_critere`
--
ALTER TABLE `eval_critere`
  MODIFY `idCritere` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT pour la table `eval_eleve`
--
ALTER TABLE `eval_eleve`
  MODIFY `idEleve` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=335;
--
-- AUTO_INCREMENT pour la table `eval_grille`
--
ALTER TABLE `eval_grille`
  MODIFY `idGrille` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `eval_prof`
--
ALTER TABLE `eval_prof`
  MODIFY `idProf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `eval_classe`
--
ALTER TABLE `eval_classe`
  ADD CONSTRAINT `eval_classe_ibfk_1` FOREIGN KEY (`refProf`) REFERENCES `eval_prof` (`idProf`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `eval_eleve`
--
ALTER TABLE `eval_eleve`
  ADD CONSTRAINT `eval_eleve_ibfk_1` FOREIGN KEY (`classeEleve`) REFERENCES `eval_classe` (`idClasse`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `eval_grille`
--
ALTER TABLE `eval_grille`
  ADD CONSTRAINT `eval_grille_ibfk_1` FOREIGN KEY (`refProf`) REFERENCES `eval_prof` (`idProf`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `eval_note`
--
ALTER TABLE `eval_note`
  ADD CONSTRAINT `eval_note_ibfk_1` FOREIGN KEY (`refNoteur`) REFERENCES `eval_eleve` (`idEleve`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eval_note_ibfk_2` FOREIGN KEY (`refEleve`) REFERENCES `eval_eleve` (`idEleve`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `eval_note_ibfk_3` FOREIGN KEY (`refCritere`) REFERENCES `eval_critere` (`idCritere`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;