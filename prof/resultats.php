<?php 
require './inc/header.php';


if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{


    //création des manager
    $eleveManager = new eleveManager(database::getDB());
    $noteManager = new noteManager(database::getDB());
    $critereManager = new critereManager(database::getDB());
    $classeManager = new classeManager(database::getDB());
    $evaluationManager = new evaluationManager(database::getDB());

    
    //si on veut réinitialiser les notes ---------------plus utilisé ?
    if(isset($_GET["reinitialiser"])){      
        $result = $noteManager->reinitialiserNotesProf($_SESSION["auth"]);
        if($result){
            $_SESSION["flash"]['success'] = "Réinitialisation effectuée";
            header("Location: resultats.php");
        }
    }
   
    //recup liste éleves de ce prof
    $tabClasses = $classeManager->getList("WHERE refProf=".$_SESSION["auth"]->getId());
    $evaluationChoisie = isset($_GET["evaluation"])?$evaluationManager->get($_GET["evaluation"]):(isset($_POST["evaluation"])?$evaluationManager->get($_POST["evaluation"]):null);
    
    if(sizeof($tabClasses)!=0 && !is_null($evaluationChoisie) && in_array($evaluationChoisie->getClasse(),$tabClasses)){
        
        //recup liste éleves de ce prof
        $tabEleves = $eleveManager->getList("WHERE classeEleve=".$evaluationChoisie->getClasse()->getId());
        //recup liste criteres de la grille active de ce prof
        $tabCriteres = $critereManager->getList("WHERE refEvaluation=".$evaluationChoisie->getId());
  
        //si on veut supprimer une note
        if(isset($_GET["suppNote"])&& isset($_GET["eleve"])&&$_GET["eleve"]!=""){ 
            $eleveNoteSupp= $eleveManager->get($_GET["eleve"]);
            $noteur= $eleveManager->get($_GET["noteur"]);
            //on vérifie que l'élève et le noteur sont bien un du prof connecté
            if (in_array($eleveNoteSupp, $tabEleves) && in_array($noteur, $tabEleves)) {
            $result = $noteManager->reinitialiserNotesProfEleve($_SESSION["auth"],$eleveNoteSupp,$noteur);
            if($result){
                $_SESSION["flash"]['success'] = "Note supprimée";
                header("Location: resultats.php");
            }
            }
            
            
        }
        
        $d = new DateTime($evaluationChoisie->getDate());
        ?>
        <h2><?= $evaluationChoisie->getNom()?></h2>
        <h4>Classe : <?=$evaluationChoisie->getClasse()->getNom()?> - <?= $d->format("d/m/Y");?></h4>
        <p>Moyennes des notes reçues par critère, pour chaque élève interrogé</p>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th>Elève interrogé</th>
                    <?php
                    foreach ($tabCriteres as $critere){
                        echo "<th>".$critere->getNom() ."</th>";
                    }
                    ?>
                    <th>Total</th>
                </tr>
            </thead>
            <?php 
            //si on veut voir les résultats
            foreach ($tabEleves as $eleveNote){
                $total=0;
                ?>
                <tr>
                    <td><?=$eleveNote->getNom()?></td>
                <?php
                foreach ($tabCriteres as $critere){
                    $note = $critereManager->getMoyenne($critere,$eleveNote);
                    $total+=$note;
                    echo "<td>".$note."</td>";
                }
                ?>
                <td><?=$total?></td>
                <td><a href="detailResultats.php?evaluation=<?=$evaluationChoisie->getId()?>&eleve=<?=$eleveNote->getId()?>">detail</a></td>
            </tr>
        <?php 
        }?>
        </table>
        <form method="POST" action="index.php">
            <button type="submit"class="form-control"> Retour</button>
        </form>
    <?php
    }else{
        if(sizeof($tabClasses)==0)
            echo "<h3>Merci de choisir une évaluation pour accéder aux résultats </h3>";
        else
            echo "<h3>Merci de choisir une évaluation qui vous appartient pour accéder aux résultats </h3>";
    }
        ?>

<?php
}
require './inc/footer.php'; ?>     