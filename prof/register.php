<?php
require './inc/header.php';

$name="";
$mail="";

if(!empty($_POST)){
    $name=$_POST['username'];
    $mail=$_POST['email'];

    $errors=array();
    $profManager = new profManager(database::getDB());
    
    if(empty($_POST['username']) || !preg_match('/^[A-Za-z0-9_ ]+$/',$_POST['username'])){
        $errors["username"]= "Nom d'utilisateur invalide";
    }/*else{
       //on vérifie si ce username est pris
        if($profManager->existeLogin($_POST['username'])){
            $errors["username"]= "Nom d'utilisateur déja pris";
        }
    }
*/
    if(empty($_POST['email']) || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
        $errors["email"]= "Email invalide";
    }else{
        //on vérifie si ce mail est pris
        if($profManager->existeMail($_POST['email'])){
            $errors["email"]= "Email déja pris";
        }
    }
    
    if(empty($_POST['password']) || strlen($_POST['password'])<=5){
        $errors["password_length"]= "Longueur de mot de passe insuffisante (min 5 caractères)";
    }
    if($_POST['password']!=$_POST['password_confirm']){
        $errors["password"]= "Les mots de passes saisis ne concordent pas";
    }

    
    if(empty($errors)){
        $pass=password_hash($_POST['password'], PASSWORD_BCRYPT);
        $token = str_random(60);
        $id= $profManager->save(new prof($_POST['username'], $_POST['email'], $pass,$token));

        require './inc/envoieMail.php';
        //mail($_POST['email'], "Confirmation de compte EvalOral", "Afin de valider votre compte, merci de cliquer sur ce lien \n\n http://evaluation.donet.yt/prof/confirm.php?id=".$id."&token=".$token);
        //echo '<a href="http://localhost/evaluation/prof/confirm.php?id='.$id.'&token='.$token.'">lien</a>';
        //$_SESSION["flash"]['success'] = "Un email de confirmation vous a été envoyé pour valider votre compte.<br> Merci de vérifier dans le courrier indésirables s'il n'est pas dans la boite de réception.";
        //header('Location: login.php');
        //exit();
    }
    
    
    }
?>

<h1> S'inscrire</h1>

<?php if (!empty($errors)): ?>
<div class="alert alert-danger">
    <p>Vous n'avez pas rempli le formulaire correctement</p>
    <ul>
        <?php foreach ($errors as $error) : ?>  
        <li><?= $error;?> </li>
        <?php endforeach; ?>
    </ul>    
</div>
<?php endif; ?>

<form method="POST">

<div class="form-group"> 

    <label for="username">Nom et prénom</label>
    <input type="text" class="form-control" name="username" value="<?= $name?>" required/>
</div>        
    
<div class="form-group"> 

    <label for="email"> Email</label>
    <input type="email" class="form-control" name="email"  value="<?= $mail?>" required/>
</div>        
<div class="form-group"> 

    <label for="password"> Mot de passe</label>
    <input type="password" class="form-control" name="password" required/>
</div>
<div class="form-group"> 

    <label for="password_confirm"> Confirmation de mot de passe</label>
    <input type="password" class="form-control" name="password_confirm" required/>
</div>
    <button type="submit" class="btn btn-primary">M'inscrire</button>
    
    
</form>




<?php require './inc/footer.php'; ?>
