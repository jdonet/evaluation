  <?php 
     
require './inc/header.php';


if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{

    //création des manager
$critereManager = new critereManager(database::getDB());
$noteManager = new noteManager(database::getDB());
$grilleManager = new grilleManager(database::getDB());
$tabGrilles = $grilleManager->getList("WHERE refProf=".$_SESSION["auth"]->getId());


//si on veut réinitialiser les notes
if(isset($_GET["reinitialiser"])){      
     $result = $noteManager->reinitialiserNotesProf($_SESSION["auth"]);
     if($result){
        $_SESSION["flash"]['success'] = "Réinitialisation effectuée";
        header("Location: gestionCriteres.php");
        
     }
     
}



//si on veut supprimer un critere
if(isset($_POST["suppCritere"])){
     $critereManager->delete($critereManager->get($_POST["suppCritere"]));
     $_SESSION["flash"]['success'] = "Suppression effectuée";
     header("Location: gestionCriteres.php");     
}
//si on veut ajouter un critere
if(isset($_POST["ajoutNomCritere"])){      
    //recup de la grille
    $grillePOST = $grilleManager->get($_POST["ajoutGrilleCritere"]);
    $critereManager->save(new critere($_POST["ajoutNomCritere"],$_POST["ajoutPointsCritere"],1,$grillePOST));
    $_SESSION["flash"]['success'] = "Ajout effectué";
    header("Location: gestionCriteres.php");

}
//si on veut modifier un critere
if(isset($_POST["modifierIdCritere"])){
    //recup de la grille
    $grillePOST = $grilleManager->get($_POST["modifierGrilleCritere"]);
    $crit = new critere($_POST["modifierNomCritere"],$_POST["modifierPointsCritere"],1,$grillePOST);
    $crit->setId($_POST["modifierIdCritere"]);
    $critereManager->save($crit);
    $_SESSION["flash"]['success'] = "Mise à jour effectuée";
    header("Location: gestionCriteres.php");

}

//recup liste criteres
$tabCriteres = $critereManager->getList(",eval_grille WHERE refGrille=idGrille AND refProf=".$_SESSION["auth"]->getId());

//si on a déja créé des grilles
if(!empty($tabGrilles)){
    
    ?>
<h2>Gestion des critères</h2>

<div class="form-inline">
    <form method="POST">
       <label for="afficherCritere">Choix du critere à modifier ou supprimer </label><br>
             <select name="choixCritere" onchange="this.form.submit();"class="form-control">
                 <option value="">Choisir un critère à modifier / supprimer </option>
             <?php 
             foreach ($tabCriteres as $critere){
                echo '<option value="'.$critere->getId().'">'.$critere->getNom().'</option>';
             }
              ?>
             </select> 
             <br> Nb total points pour tous les critères: <?php echo $critereManager->getTotalPoints(",eval_grille WHERE refGrille=idGrille AND refProf=".$_SESSION["auth"]->getId());?><br><br>
    </form>
 </div>


  <?php 
  //si on veut gérer les criteres
    if(isset($_POST["choixCritere"])){
       $critereChoisi= $critereManager->get($_POST["choixCritere"]);
  ?>
<br><br>
<h4>Attention : Une modification de critère (barème, ajout ou suppression de critère) peut fausser le calcul final. Il est recommandé de <a href="gestionCriteres.php?reinitialiser">supprimer vos anciennes notes</a></h4>
<br><br><br>
    <div class="modification">
         <h3>Modifier le critère <?php  echo  $critereChoisi->getNom();?></h3>
         <form method="POST" class="form-check">
             <input type="hidden" name="modifierIdCritere" value="<?php  echo  $critereChoisi->getId();?>" class="form-control">
             <input type="text" required name="modifierNomCritere" placeholder="nom du critere"value="<?php  echo  $critereChoisi->getNom();?>" class="form-control">
             <input type="number" required name="modifierPointsCritere" placeholder="nb points max (entier)" value="<?php  echo  $critereChoisi->getPointMax();?>" class="form-control">
             <select name="modifierGrilleCritere" class="form-control">
            <?php 
            foreach ($tabGrilles as $grille){
            echo '<option value="'.$grille->getId().'">'.$grille->getNom().'</option>';
            }
            ?>
            </select>
             <input type="submit" value="valider modification" class="form-control">
         </form><br>
    </div>
    <div class="suppression">
       <h3>Supprimer <?php  echo  $critereChoisi->getNom();?></h3>
       <form method="POST" class="form-check">
             <input type="hidden" name="suppCritere" value="<?php  echo  $critereChoisi->getId();?>" class="form-control">
             <input type="submit" value="supprimer ce critère (Attention)" class="form-control">
         </form><br>
    </div>
  <?php 
    }
  ?>
<div class="ajout">
    <h2>Ajout d'un nouveau critère </h2>
     <form method="POST" class="form-check">

        <input type="text" required name="ajoutNomCritere" placeholder="nom du critere" class="form-control">
        <input type="number" required name="ajoutPointsCritere" placeholder="nb points max (entier)" class="form-control">
        <select name="ajoutGrilleCritere" class="form-control">
        <?php 
        foreach ($tabGrilles as $grille){
           echo '<option value="'.$grille->getId().'">'.$grille->getNom().'</option>';
        }
        ?>
        </select>
        <input type="submit" value="ajouter critère" class="form-control">
    </form><br>
</div>
           
            
<?php


}else { ?>
    <div>
     <h3>Merci d'ajouter d'abord une grille</h3>
    </div> 
    <?php
}
}

require './inc/footer.php'; ?>