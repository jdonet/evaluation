<?php
require './inc/header.php';

//création des manager
$grilleManager = new grilleManager(database::getDB());
$critereManager = new critereManager(database::getDB());


if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{


//si on veut supprimer une grille
if(isset($_GET["suppGrille"])){
    $grilleSup = $grilleManager->getList("WHERE refProf=".$_SESSION["auth"]->getId() ." AND idGrille = ".$_GET["suppGrille"])[0];
    $grilleManager->delete($grilleSup);
    $_SESSION["flash"]['success'] = "Suppression effectuée";
    header("Location: gestionGrilles.php");
 
}

//si on veut ajouter ou modifier une grille
if(isset($_POST['modifierIdGrille']) && !empty($_POST['modifierIdGrille'])
    && isset($_POST['modifierNomGrille']) && !empty($_POST['modifierNomGrille'])){
    
    $idCritere = $_POST['idCritere']??null;
    $nomCritere = $_POST['nomCritere']??null;
    $nbPointsCritere = $_POST['nbPointsCritere']??null;
    
    //modification de l'ancienne grille
    $grille = new grille($_POST["modifierNomGrille"],$_POST["modifierActiveGrille"],$_SESSION["auth"]);
    $grille->setId($_POST['modifierIdGrille']);
    $grilleManager->save($grille);

    //suppresion des critères effacés (ceux de la grille avant qui n'y sont plus maintenant)
    $lesCriteres = $critereManager->getList("WHERE refGrille=".$_POST["modifierIdGrille"]); //recup les critères de la grille en BDD

    for($i=0;$i<count($lesCriteres);$i++){ //supp des anciens critères en bdd
            $critereManager->delete($lesCriteres[$i]);
    }
    
    //maj liste critères
    for($i=0;$i<count($nomCritere);$i++){
        if(!empty($nbPointsCritere[$i]) && !empty($nomCritere[$i])){
            $critere = new critere($nomCritere[$i],$nbPointsCritere[$i],0,$grille,null);
            //si le critere existe
            if (isset($idCritere[$i]) && $idCritere[$i]!="")
                $critere->setId($idCritere[$i]);
            $critereManager->save($critere);
        }
    }
    $_SESSION["flash"]['success'] = "Mise à jour effectuée";
    header("Location: gestionGrilles.php");
        
}

    
//recup liste grilles
$tabGrilles = $grilleManager->getList("WHERE refProf=".$_SESSION["auth"]->getId());


    
?>
<h2>Gestion des grilles</h2>
<div class="form-inline">
       <div class="card-group">
            <?php 
            $i=0;
            foreach ($tabGrilles as $grille){
                if($i%3==0){
                    echo "</div><div class='card-group'>";
                }
                $i++;
                ?>
                <form method="POST" action="editerGrille.php">
                    <div class="card" style="width: 18rem; height: 355.833px;">
                        <?php //afichage des 4 premiers critères
                        echo $grilleManager->afficher4PremiersCriteres($grille);
                        ?>
                        <div class="card-body">
                            <input type="hidden" name="idGrille" value="<?php  echo  $grille->getId();?>">
                            <h5 class="card-title"><?= $grille->getNom() ?></h5>
                            <button class="btn btn-outline-dark" type="submit">Modifier</button>
                            <button class="btn btn-outline-dark" type="button" onclick="supprimer('cette grille','gestionGrilles.php?suppGrille='+<?= $grille->getId()?>)">Supprimer</button>
                        </div>
                    </div>
                </form>
                <?php
            }
            
            if($i%3 ==0){
                ?>
                </div><div class='card-group'>
                <form method="POST" action="editerGrille.php">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="../img/ajouter.png" alt="Ajouter une grille">
                        <div class="card-body">
                            <h5 class="card-title">Ajouter une grille</h5>
                            <button class="btn btn-outline-dark" type="submit">Ajouter</button>
                        </div>
                    </div>
                </form>
                <div class="card" style="width: 18rem;">
                </div>
                <div class="card" style="width: 18rem;">
                </div>
                <?php
            }else if($i%3 ==1){
                ?>
                <form method="POST" action="editerGrille.php">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="../img/ajouter.png" alt="Ajouter une grille">
                        <div class="card-body">
                            <h5 class="card-title">Ajouter une grille</h5>
                            <button class="btn btn-outline-dark" type="submit">Ajouter</button>
                        </div>
                    </div>
                </form>
                <div class="card" style="width: 18rem;">
                </div>
                <?php
            }else{
                ?>
                <form method="POST" action="editerGrille.php">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="../img/ajouter.png" alt="Ajouter une grille">
                        <div class="card-body">
                            <h5 class="card-title">Ajouter une grille</h5>
                            <button class="btn btn-outline-dark" type="submit">Ajouter</button>
                        </div>
                    </div>
                </form>
                <?php
            }?>
        </div>
 </div>
           

<?php

}
require './inc/footer.php'; ?>