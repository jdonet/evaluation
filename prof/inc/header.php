<?php 

require_once './inc/functions.php';

if(session_status()== PHP_SESSION_NONE){
    session_start();     
}
function __autoload($class_name){
    require('../classes/' . $class_name . '.class.php'); 
}
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Evaluation d'oral - prof</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- julien CSS -->
    <link rel="stylesheet" media="screen" href="../css/style.css" type="text/css" />
    
    <!-- script -->
    <script src="../script/jquery-3.3.1.js"></script>

    <!-- julien JS -->
    <script src="../script/background.js" type="text/javascript"></script>
    <script src="../script/tableau.js" type="text/javascript"></script>
    
  </head>

  <body data-spy="scroll">
    <img src="../img/fond.jpg" class="superbg" />
    <nav class="navbar navbar-expand-lg navbar-light bg-light container">
      <a class="navbar-brand" href="../index.php">EvalOral</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <?php 
            if(isset($_SESSION["auth"])){ ?>
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Evaluations</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="index.php">Liste</a>
                                <a class="dropdown-item" href="ajouterEvaluation.php">Ajouter</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Configuration</a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="gestionClasses.php">Classes</a>
                                <a class="dropdown-item" href="gestionGrilles.php">Grilles</a>
                            </div>
                        </li>
                    </ul>
                    <ul class="nav  navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="../eleve.php">Interface élève</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><?= $_SESSION["auth"]->getNom()?></a>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="logout.php">Déconnecter</a>
                                <a class="dropdown-item" href="account.php">Votre compte</a>
                            </div>
                        </li>
                    </ul>
                
            <?php 
            }else{
            ?>
                    <ul class="nav navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="../eleve.php">Interface élève</a>
                        </li>
                    </ul>
                <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="register.php">S'inscrire</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="login.php">Se connecter</a>
                        </li>
                    </ul>
                
            <?php 
            }
            ?>
    </div>
  </nav>
        
    <div id="contenu">
        <?php 
                
        //affichage des messages d'erreur
        if(isset($_SESSION["flash"])){
            foreach ($_SESSION["flash"] as $type => $message){
                echo '<div class="alert-'.$type.'">'.$message.'</div>';
            }    
            unset($_SESSION["flash"]);
        }
       
?>