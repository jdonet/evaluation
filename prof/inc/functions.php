<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function debug($variable){
    echo '<pre>'.print_r($variable,true).'</pre>';
    
}

function str_random($length){
    $alphabet = "0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN";
    return substr(str_shuffle(str_repeat($alphabet, $length)), 0, $length);
}

function h($input) {
    $new=strip_tags($input);
    $new = htmlspecialchars($new, ENT_QUOTES);
    return $new;
    }