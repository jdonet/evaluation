<!-- Modal -->
        <div class="modal fade" id="exampleModal<?=$i?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?=$i?>" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel<?=$i?>">Qui a noté?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <h3>A noté</h3>
                    <?php   
                    foreach ($tabEleves as $eleve){
                        if($noteManager->aNote($eleve,$eleveActif,$eval) ==1)
                            echo $eleve->getNom()."<a href='index.php?suppNote&noteur=".$eleve->getId()."&eleve=".$eleveActif->getId()."&evaluation=".$eval->getId()."'><img alt='supprimer note' width='20px' src='../img/supprimer.jpg'></a><br>";
                    }
                    ?>
                    <br><br><br>
                <h3>N'a pas noté</h3>
                    <?php 
                    foreach ($tabEleves as $eleve){
                        if($noteManager->aNote($eleve,$eleveActif,$eval) ==0)
                            echo $eleve->getNom()."<br>";
                    }
                    ?>        
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
              </div>
            </div>
          </div>
        </div>