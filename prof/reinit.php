<?php 
require './inc/header.php';


if (isset($_GET['id']))
    $user_id=$_GET['id'];
else if(isset($_POST['id']))
    $user_id=$_POST['id'] ;
else $user_id="";

if(isset($_GET['token']))
    $token=$_GET['token'];
else if(isset($_POST['token']))
    $token=$_POST['token'];
else
    $token="";

$profManager = new profManager(database::getDB());
$user= $profManager->get($user_id);

if ($user and $user->getToken()==$token){

    if(isset($_POST)){ //formulaire envoyé

        if(empty($_POST['password']) || strlen($_POST['password'])<=5){
            $errors["password_length"]= "Longueur de mot de passe insuffisante (min 5 caractères)";
        }else{
            $user->setToken("");
            $pass=password_hash($_POST['password'], PASSWORD_BCRYPT);
            $user->setPass($pass);
            $profManager->save($user);

            $_SESSION["flash"]['success'] = "Mot de passe réinitialisé";
            $_SESSION["auth"]=$user;
            header("Location: login.php");
            exit();
        }
    }

?>
<h1>Nouveau mot de passe</h1>
<?php if (!empty($errors) && isset($_POST["password"])): ?>
<div class="alert alert-danger">
    <p>Vous n'avez pas rempli le formulaire correctement</p>
    <ul>
        <?php foreach ($errors as $error) : ?>  
        <li><?= $error;?> </li>
        <?php endforeach; ?>
    </ul>    
</div>
<?php endif; ?>

<form method="POST">
    <input type="hidden" class="form-control" name="id" value="<?=$user_id?>"/>
    <input type="hidden" class="form-control" name="token" value="<?=$token?>"/>
    <div class="form-group"> 
        <label for="username">Nouveau mot de passe</label>
        <input type="password" class="form-control" name="password"/>
    </div>        
    <button type="submit" class="btn btn-primary">Réinitialiser le mot de passe</button>    

<?php
}else{
    $_SESSION["flash"]["danger"]="Ce token n'est plus valide";
    header("Location: login.php");
}



require './inc/footer.php'; ?>
