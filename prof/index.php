<?php 
require './inc/header.php';

if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{
    //création des manager
    $eleveManager = new eleveManager(database::getDB());
    $noteManager = new noteManager(database::getDB());
    $critereManager = new critereManager(database::getDB());
    $classeManager = new classeManager(database::getDB());
    $grilleManager = new grilleManager(database::getDB());
    $evaluationManager = new evaluationManager(database::getDB());

    $tabEval = $evaluationManager->getList("WHERE refProf= ".$_SESSION["auth"]->getId());
    //suppression evaluation si elle appartient bien au prof connecté
    if(isset($_GET["suppEval"]) && $_GET["suppEval"]!=""){
        $e = $evaluationManager->get($_GET["suppEval"]);
        $idClasse = $e->getClasse()->getId();
        $e!=false && in_array($e,$tabEval)?$evaluationManager->delete($e):"";

        $_SESSION["flash"]['success'] = "Suppression effectuée";
        header("Location: index.php");

    }
    
    //activer evaluation si elle appartient bien au prof connecté
    if(isset($_GET["activerEvaluation"]) && $_GET["activerEvaluation"]!=""){
        $e = $evaluationManager->get($_GET["activerEvaluation"]);
        $idClasse = $e->getClasse()->getId();
        if($e!=false && in_array($e,$tabEval)){
            //on va d'abord archiver toutes les évaluations de cette classe
            $evaluations = $evaluationManager->getList(" WHERE refClasse=".$idClasse." AND archiveEvaluation=0 AND refProf=".$_SESSION["auth"]->getId());
            foreach ($evaluations as $eval){
                $eval->setArchive(1);
                $evaluationManager->save($eval);
            }
        
            //puis activer celle désirée
            $e->setArchive(0);
            $evaluationManager->save($e);
        }
        $_SESSION["flash"]['success'] = "Evaluation activée";
        header("Location: index.php");

    }

    $tabEval = $evaluationManager->getList("WHERE refProf= ".$_SESSION["auth"]->getId());
    //recup liste éleves
    $tabClasses = $classeManager->getList(" WHERE refProf=".$_SESSION["auth"]->getId());

    if(count($tabClasses)==0){ //s'il n'a pas de classe
        header("Location: gestionClasses.php");
        exit();
    }
    //recup liste éleves de ce prof
    $tabEleves = $eleveManager->getList("WHERE classeEleve = ". $tabClasses[0]->getId());

    
    


    //modif eleve actif s'il est bien un des éleves du prof connecté
    if(isset($_POST["activerEleve"])){
        $e = $eleveManager->get($_POST["activerEleve"]);
        $tabEleves = $eleveManager->getList(",eval_classe WHERE classeEleve = idClasse AND refProf= ".$_SESSION["auth"]->getId());
        $e!=false && in_array($e,$tabEleves)?$eleveManager->activer($e):"";
        $_SESSION["flash"]['success'] = "Modification élève actif effectuée";
        header("Location: index.php");

    }
    //si on veut supprimer une note
    if(isset($_GET["suppNote"]) && isset($_GET["eleve"]) && $_GET["eleve"]!="" && isset($_GET["evaluation"]) && $_GET["evaluation"]!=""){ 
        $eleveNoteSupp= $eleveManager->get($_GET["eleve"]);
        $noteur= $eleveManager->get($_GET["noteur"]);
        $evaluation= $evaluationManager->get($_GET["evaluation"]);
        //on vérifie que l'élève, le noteur et l'évaluation sont bien un du prof connecté
        if (in_array($eleveNoteSupp, $tabEleves) && in_array($noteur, $tabEleves) && in_array($evaluation, $tabEval)) {
            $result = $noteManager->reinitialiserNotesProfEleve($_SESSION["auth"],$eleveNoteSupp,$noteur,$evaluation);
            if($result){
                $_SESSION["flash"]['success'] = "Suppression effectuée";
                header("Location: index.php");
            }
        }   
    }


    
    //recup liste criteres de ce prof
    $tabCriteres = $critereManager->getList(",eval_grille WHERE refEvaluation IS NULL AND refGrille=idGrille AND refProf=".$_SESSION["auth"]->getId());
    foreach ($tabCriteres as $critere) {
    ?>
    <script> ajouterCritere(<?=$critere->getId()?>,'<?=$critere->getNom()?>',<?=$critere->getPointMax()?>,<?=$critere->getGrille()->getId()?>) </script>
    <?php
    }

    //recup liste grilles de ce prof
    $tabGrilles = $grilleManager->getList(" WHERE refProf=".$_SESSION["auth"]->getId());
    
    //recup liste evaluations de ce prof
    $tabEvaluationsActives = $evaluationManager->getList(" WHERE archiveEvaluation=0 AND refProf=".$_SESSION["auth"]->getId());
    $tabEvaluationsArchives = $evaluationManager->getList(" WHERE archiveEvaluation=1 AND refProf=".$_SESSION["auth"]->getId());

    if (sizeof($tabCriteres)==0 or sizeof($tabEleves) ==0 or sizeof($tabClasses)==0 or sizeof($tabGrilles)==0  ){
        if (sizeof($tabClasses)==0  ){
            echo "<h3>Merci de saisir des classes</h3>";
        }
        if (sizeof($tabEleves) ==0){
            echo "<h3>Merci de saisir des élèves</h3>";
        }
        if (sizeof($tabCriteres)==0){
            echo "<h3>Merci de saisir des critères</h3>";
        }
        if (sizeof($tabGrilles)==0){
            echo "<h3>Merci de saisir des grilles</h3>";
        }
    }else{
        
        
        ?>


        <h2>Gestion des évaluations</h2>
        <div class="alert alert-info" role="alert">
            Les élèves peuvent noter uniquement sur l'évaluation active. <br>Choisissez l'élève qui passe dans la liste déroulante.
        </div>       
        <div>
            <h4>Evaluations actives</h4>
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr><th>Date</th><th>Classe</th><th>Nom</th><th>Elève interrogé</th><th>Notes</th></tr>
                </thead>
                    <?php 
                    $i=0;
                    foreach ($tabEvaluationsActives as $eval){
                    ?>
                        <form method="POST">
                            <tr>
                                <td><?php $d = new DateTime($eval->getDate());
                                echo $d->format("Y-m-d"); ?></td>
                                <td>
                                    <?= $eval->getClasse()->getNom() ?>
                                </td>
                                <td>
                                    <?= strlen($eval->getNom())>30?substr ( $eval->getNom(),0 ,30 )."...":$eval->getNom() ?>
                                </td>
                                <td>
                                    <select name="activerEleve" onchange="this.form.submit();"class="form-control">
                                        <?php 
                                        $tabEleves = $eleveManager->getList("WHERE classeEleve = ". $eval->getClasse()->getId());
                                        $eleveActif=null;
                                        foreach ($tabEleves as $eleve){
                                            if($eleve->getActif()==1){
                                                echo '<option selected value="'.$eleve->getId().'">'.$eleve->getNom().'</option>';
                                                $eleveActif=$eleve;
                                            }
                                            else
                                                echo '<option value="'.$eleve->getId().'">'.$eleve->getNom().'</option>';
                                        }
                                        ?>
                                    </select>
                                    <?php //si aucun élève actif, on prend le premier de la liste
                                    if(is_null($eleveActif)){
                                        $eleveManager->activer($tabEleves[0]);
                                        $eleveActif=$tabEleves[0];
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="resultats.php?evaluation=<?= $eval->getId() ?>">notes</a>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?= $i ?>">
                                    Qui a noté?
                                    </button>
                                </td>
                                <td>
                                    <button class="btn btn-outline-dark btn-sm" type="button" onclick="supprimer('cette évaluation','index.php?suppEval='+<?= $eval->getId()?>)">X</button>
                                </td>
                            </tr>
                        </form>
                        <?php include 'inc/quiANote.php'; ?>
                    <?php 
                    $i++;
                    }
                    ?>

                </table>                     
         </div>
         <br><br>
         <div>
            <h4>Evaluations archivées</h4>
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr><th>Date</th><th>Classe</th><th>Nom</th><th>Moy</th><th>+</th><th>-</th><th>Activer</th><th>Notes</th></tr>
                </thead>
                <?php 
                foreach ($tabEvaluationsArchives as $eval){
                ?>
                    <tr>
                        <td><?php $d = new DateTime($eval->getDate());
                            echo $d->format("Y-m-d"); ?></td>
                        <td>
                            <?= $eval->getClasse()->getNom() ?>
                        </td>
                        <td>
                            <?= strlen($eval->getNom())>30?substr ( $eval->getNom(),0 ,30 )."...":$eval->getNom() ?>
                        </td>
                        <td>
                            <?= $evaluationManager->getMoyenne($eval); ?>
                        </td>
                        <td>
                            <?= $evaluationManager->getMax($eval);?>
                        </td>
                        <td>
                            <?= $evaluationManager->getMin($eval);?>
                        </td>
                        <td>
                            <a href="index.php?activerEvaluation=<?= $eval->getId() ?>"><img src="../img/activer.png" title="activer évaluation" width="20px"></a>
                        </td>
                        <td>
                        <a href="resultats.php?evaluation=<?= $eval->getId() ?>">notes</a>
                        </td>
                        <td>
                        <button class="btn btn-outline-dark btn-sm" type="button" onclick="supprimer('cette évaluation','index.php?suppEval='+<?= $eval->getId()?>)">X</button>
                        </td>
                    </tr>
                <?php 
                }
                ?>
            </table>
         </div>
        <?php
    }
}
require './inc/footer.php'; ?>