<?php
require './inc/header.php';

//création des manager
$grilleManager = new grilleManager(database::getDB());
$critereManager = new critereManager(database::getDB());

if (!isset($_SESSION["auth"])) {
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
} else {
    if (isset($_POST["idGrille"])) { //recup grille
        $grilleChoisie = $grilleManager->getList("WHERE refProf=".$_SESSION["auth"]->getId() ." AND idGrille = ".$_POST["idGrille"])[0];
        $criteresGrille = $critereManager->getList("WHERE refGrille = ".$grilleChoisie->getId());

    }else{ //on créé une nouvelle grille
        $grilleChoisie = new grille("Ma grille",1,$_SESSION["auth"]);
        $grilleManager->save($grilleChoisie);
        $criteresGrille = array();
    }

    if ($grilleChoisie) {

    ?>
        
        <div class="form-inline">
            <form method="POST" id="formCritere" action="gestionGrilles.php" class="center-items">
            <br><br>
                <h2>Nom de la grille <input type="text" name="modifierNomGrille" placeholder="nom de la grille" value="<?= $grilleChoisie->getNom(); ?>" class="form-control" required></h2>
                <br><br>
                <h3>Liste des critères </h3>
                <input type="hidden" name="modifierIdGrille" value="<?php  echo  $grilleChoisie->getId(); ?>" class="form-control">
                <input type="hidden" name="modifierActiveGrille" value="<?php  echo  $grilleChoisie->getActive(); ?>" class="form-control">
                <table class="table" id="tableCriteres">
                    <thead>
                        <tr>
                        <th scope="col">Nom</th>
                        <th scope="col">Nb points</th>
                        <th scope="col">Suppression</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($criteresGrille as $critere) {

                        ?>
                                
                        <tr>
                            <td><input type="hidden" name="idCritere[]" value="<?= $critere->getId()?>"><input type="text" name="nomCritere[]" placeholder="nom du critère"value="<?= $critere->getNom()?>" required class="form-control" size="10"></td>
                            <td><input type="number" min="1" name="nbPointsCritere[]" placeholder="nombre de points du critère"value="<?= $critere->getPointMax()?>" class="form-control" size="10"></td>
                            <td><button type="button" onclick="supprimeLigne(this.parentNode.parentNode);"><img alt='supprimer critere' width='20px' src='../img/supprimer.jpg'></button></td>
                        </tr>
                        <?php
                    }
                    ?>
                        <tr>
                            <td colspan="4"></td>
                            <td><input type="button" value="ajouter" onclick ="ajouteLigne()" class="form-control"></td>
                        </tr>
                </table>
                <br><br>
                <a href="gestionGrilles.php" class="form-control" aria-disabled="true">Annuler</a>
                <input type="submit" value="Valider" class="form-control">
            </form>
        </div>

        <?php
       
    } else {
        header('Location: gestionGrilles.php'); //on est arrivé sur la page sans avoir sélectionné de grille
    }
}
require './inc/footer.php'; ?>