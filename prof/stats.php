<?php 
require './inc/header.php';


if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{


    //création des manager
    $profManager = new profManager(database::getDB());
    $classeManager = new classeManager(database::getDB());
    $evaluationManager = new evaluationManager(database::getDB());
    $loginManager = new loginManager(database::getDB());


    $tabProfs = $profManager->getList();

?>
<h1>Profs</h1>

<table class="table">
<tr><th>Nom</th><th>Nb classes</th><th>NbEval</th><th>nbCnx</th><th>LastCnx</th></tr>
<?php
foreach ($tabProfs as $prof){
    echo "<tr><td>".$prof->getNom()."</td><td>".$classeManager->getNbClasses($prof)."</td><td>".$evaluationManager->getNbEvaluations($prof)."</td><td>".$loginManager->getNbCnx($prof)."</td><td>".$loginManager->getLastCnx($prof)."</td></tr>";
}
?>
</table>
<?php
}
require './inc/footer.php'; ?>     