<?php 
require './inc/header.php';

if(!empty($_POST) and !empty($_POST["username"]) and !empty($_POST["password"])){
    $profManager = new profManager(database::getDB());
    $user= $profManager->verifLogin($_POST['username']);
    //si utilisateur trouvé
    if ($user != false){
        //verification du pass crypté
        if(password_verify($_POST['password'], $user->getPass())){
            $loginManager = new loginManager(database::getDB());
            $_SESSION["token"] = $loginManager->getToken($user->getId(),null);
            $_SESSION["auth"]=$user;
            $_SESSION["flash"]['success'] = "Connexion réussie";
            header('Location: index.php');
            exit();
        }else{
        $_SESSION["flash"]["danger"]="Mot de passe incorrect";
        }
    }else{
        $_SESSION["flash"]["danger"]="Email introuvable";
    }
    header('Location: login.php');
    exit();
}
?>

<h1>Interface prof</h1>

<br><br>
<h3> Se connecter</h3>
<form method="POST">
    <div class="form-group"> 

        <label for="username">Adresse email</label>
        <input type="email" class="form-control" name="username"/>
    </div>        
        
    <div class="form-group"> 

        <label for="password"> Mot de passe</label>
        <input type="password" class="form-control" name="password"/>
    </div>
        <button type="submit" class="btn btn-primary">Connexion</button>    
</form>
<br><br>
<form method="POST" action="passForgotten.php" >
    <div class="form-group"> 
        <button type="submit" class="btn  btn-outline-dark btn-sm">Mot de passe oublié</button>    
</form>

<?php require './inc/footer.php'; ?>
