<?php
require './inc/header.php';

//création des manager
$classeManager = new classeManager(database::getDB());
$eleveManager = new eleveManager(database::getDB());

if (!isset($_SESSION["auth"])) {
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
} else {
    if (isset($_POST["idClasse"])) { //recup classe
        $classeChoisie = $classeManager->getList("WHERE refProf=".$_SESSION["auth"]->getId() ." AND idClasse = ".$_POST["idClasse"])[0];
        $elevesClasse = $eleveManager->getList("WHERE classeEleve = ".$classeChoisie->getId());
    }else{ //on créé une nouvelle classe
        $classeChoisie = new classe("Ma classe",$_SESSION["auth"]);
        $classeManager->save($classeChoisie);
        $elevesClasse = array();
    }

    if ($classeChoisie) {

    ?>
        <form method="POST" class="center-items" enctype="multipart/form-data">
            <div class="form-group">
                <input type="hidden" name="idClasse" value="<?php  echo  $classeChoisie->getId(); ?>" class="form-control">
                <label for="files">Import élève via fichier (<a href="inc/eleve.csv">voir format</a> ):</label>
                <input type="file" id="file" name="file" class="form-control-file" accept=".csv" required />
            </div>
                <div class="form-group">
                <button type="submit" id="submitFile"name="submitFile" class="btn">Importer</button>
            </div>
        </form>
        <br><br>
        <div class="form-inline">
            <form method="POST" id="formEleve" action="gestionClasses.php" class="center-items">
                <h2>Gestion de la classe <input type="text" name="modifierNomClasse" placeholder="nom de la classe" value="<?= $classeChoisie->getNom(); ?>" class="form-control" required></h2>
                <div class="alert alert-danger" role="alert">
                    Une suppression d'élève vous fera perdre ses résultats.<br> 
                    Une classe sans élève sera détruite automatiquement. <br>
                </div>
                <h3>Liste des élèves </h3>
                <input type="hidden" name="modifierIdClasse" value="<?php  echo  $classeChoisie->getId(); ?>" class="form-control">
                <table class="table" id="tableEleves">
                    <thead>
                        <tr>
                        <th scope="col">Nom</th>
                        <th scope="col">Prenom</th>
                        <th scope="col">Login</th>
                        <th scope="col">Pass</th>
                        <th scope="col">Suppression</th>
                        </tr>
                    </thead>
                    <?php
                    foreach ($elevesClasse as $eleve) {

                        ?>
                        <tr>
                            <td><input type="hidden" name="idEleve[]" value="<?= $eleve->getId()?>"><input type="text" name="nomEleve[]" placeholder="nom de l'élève"value="<?= $eleve->getNom()?>" required class="form-control" size="10"></td>
                            <td><input type="text" name="prenomEleve[]" placeholder="prénom de l'élève"value="<?= $eleve->getPrenom()?>" class="form-control" size="10"></td>
                            <td><input type="email" name="loginEleve[]" placeholder="email de l'élève"value="<?= $eleve->getLogin()?>" required class="form-control" size="10"></td>
                            <td><input type="hidden" name="actifEleve[]" value="<?= $eleve->getActif()?>"><input type="text" name="passEleve[]" placeholder="pass de l'élève"value="<?= $eleve->getPass()?>" required class="form-control" size="10"></td>
                            <td><button type="button"onclick="supprimeLigne(this.parentNode.parentNode);"><img alt='supprimer eleve' width='20px' src='../img/supprimer.jpg'></button></td>
                        </tr>
                        <?php
                    }
                    ?>
                        <tr>
                            <td colspan="4"></td>
                            <td><input type="button" value="ajouter" onclick ="ajouteEleve()" class="form-control"></td>
                        </tr>
                </table>
                <a href="gestionClasses.php" class="form-control" aria-disabled="true">Annuler</a>
                <input type="submit" value="Valider" class="form-control">
                
            </form>
        </div>
        
        <?php
       
    } else {
        header('Location: gestionClasses.php'); //on est arrivé sur la page sans avoir sélectionné de classe
    }
}
if ( isset($_POST["submitFile"]) ) {

    if ( isset($_FILES["file"])) {
 
             //if there was an error uploading the file
         if ($_FILES["file"]["error"] > 0) {
             echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
         }
         else {
             //extension fichier
            $path = $_FILES['file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);

            define('MB', 1048576);
            if($_FILES["file"]["size"] <  1*MB && $ext =="csv"){
                // upload fichier
                move_uploaded_file($_FILES["file"]["tmp_name"], "upload/eleve.csv");
                $row = 0;
                $inserted=0;
                // ouverture fichier
                if (($handle = fopen("./upload/"."eleve.csv", "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                        $num = count($data);
                        if ($row!=0 && $num==4){
                            echo '<script>ajouteEleve("", "'.$data[0].'","'.$data[1].'","'.$data[2].'",0,"'.$data[3].'") </script>';
                            $inserted++;
                        }
                        $row++; 
                    }
                    fclose($handle);
                }
                // destruction fichier        
                unlink('./upload/eleve.csv');   
                if($inserted == 0 )
                    $errors["format"]= "Format de fichier incorrect. \n Merci de partir du modèle proposé et de ne pas ajouter de colonne";
            }else{
                if($_FILES["file"]["size"] >=  1*MB )
                    $errors["taille"]= "Fichier trop volumineux (max 1Mo)";
                if($ext !="csv")
                    $errors["type"]= "Mauvaise extension de fichier (csv attendu)";
            }
         }
      } else {
            $errors["fichier"]= "Aucun fichier sélectionné";
      }
 }
 if (!empty($errors)): ?>
 <div class="alert alert-danger">
     <p>Erreurs rencontrées à l'upload</p>
     <ul>
         <?php foreach ($errors as $error) : ?>  
         <li><?= $error;?> </li>
         <?php endforeach; ?>
     </ul>    
 </div>
 <?php endif;


require './inc/footer.php'; ?>