<?php
require './inc/header.php';

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$user_id = $_GET['id'];
$token = $_GET['token'];

$profManager = new profManager(database::getDB());
$user= $profManager->get($user_id);

session_start();

if ($user and $user->getToken()==$token){
    //maj de la table
    $user->setConfirmedAt(date("Y-m-d H:i:s"));
    $user->setToken("");
    $profManager->save($user);
    //message de feedback ok
    $_SESSION["flash"]['success'] = "Votre compte a bien été validé";
    
    $_SESSION["auth"]=$user;
    header("Location: index.php");
    
}else{
    $_SESSION["flash"]["danger"]="Avez-vous déjà cliqué sur ce lien ? <br> Si oui, merci de vous connecter avec votre mail/pass";
    header("Location: login.php");
}