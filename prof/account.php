<?php 
require './inc/header.php'; ?>
<?php 



if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["danger"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{

    $profManager = new profManager(database::getDB());

    //suppression du compte
    if(isset($_GET['suppCompte']) && $_GET['suppCompte'] == $_SESSION["auth"]->getId()){
        $profManager->delete($_SESSION["auth"]);
        $_SESSION["flash"]["sucess"]="Votre compte a bien été supprimé";
        header('Location: ../index.php');
    }

    //modification du compte
    if(isset($_POST['userid']) && $_POST['userid'] == $_SESSION["auth"]->getId()){

        $errors=array();
        
        if(empty($_POST['username']) || !preg_match('/^[A-Za-z0-9_ ]+$/',$_POST['username'])){
            $errors["username"]= "Nom d'utilisateur invalide";
        }
        if(empty($_POST['email']) || !filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
            $errors["email"]= "Email invalide";
        }else{
            //on vérifie si ce mail est pris
            if($_POST['email']!= $_SESSION["auth"]->getLogin() && $profManager->existeMail($_POST['email'])){
                $errors["email"]= "Email déja pris";
            }
        }
        
        //si modif pass
        if(isset($_POST["password"])&& $_POST["password"]!=""){
            if(!password_verify($_POST['password'], $_SESSION['auth']->getPass())){
                $errors["old_password"]= "L'ancien mot de passe saisi est incorrect";
            } 
            
            if(empty($_POST['new_password']) || strlen($_POST['new_password'])<=5){
                $errors["password_length"]= "Longueur de mot de passe insuffisante (min 5 caractères)";
            }
            if($_POST['new_password']!=$_POST['new_password_confirm']){
                $errors["password_check"]= "Les mots de passes saisis ne concordent pas";
            }
        }

        if(empty($errors)){
            $_SESSION["auth"]->setNom($_POST['username']);
            $_SESSION["auth"]->setLogin($_POST['email']);
            $_SESSION["auth"]->setPass(password_hash($_POST['new_password'], PASSWORD_BCRYPT));
            $id= $profManager->save($_SESSION["auth"]);
            $_SESSION["flash"]['success'] = "Votre compte a bien été modifié";
            header('Location: account.php');
            exit();
        }
    }

?>

<h1> Votre compte </h1> 
<?php if (!empty($errors)): ?>
<div class="alert alert-danger">
    <p>Vous n'avez pas rempli le formulaire correctement</p>
    <ul>
        <?php foreach ($errors as $error) : ?>  
        <li><?= $error;?> </li>
        <?php endforeach; ?>
    </ul>    
</div>
<?php endif; ?>
<form method="POST">
    <input type="hidden" name="userid" required value="<?= $_SESSION["auth"]->getId()?>"/>
<div class="form-group"> 
    <label for="username">Nom et prénom</label>
    <input type="text" class="form-control" name="username" required value="<?= $_SESSION["auth"]->getNom()?>"/>
</div>        
    
<div class="form-group"> 

    <label for="email"> Email</label>
    <input type="email" class="form-control" name="email" required  value="<?= $_SESSION["auth"]->getLogin()?>"/>
</div>        
<div class="form-group"> 
    <label for="password"> Mot de passe actuel</label>
    <input type="password" class="form-control" name="password" placeholder="A renseigner uniquement si vous souhaitez changer de mot de passe"/>
</div>
<div class="form-group"> 
    <label for="password"> Nouveau mot de passe</label>
    <input type="password" class="form-control" name="new_password"  placeholder="A renseigner uniquement si vous souhaitez changer de mot de passe" />
</div>
<div class="form-group"> 

    <label for="password_confirm"> Confirmation du nouveau mot de passe</label>
    <input type="password" class="form-control" name="new_password_confirm"  placeholder="A renseigner uniquement si vous souhaitez changer de mot de passe" />
</div>
    <button type="submit" class="btn btn-primary">Modifier mon compte</button>
</form>
<br><br><br><br><br><br><br>
<form method="POST" action="editerClasse.php">
    <button class="btn btn-outline-dark" type="button" onclick="supprimer('votre compte!!!! \n Attention : cette action est irréversible et toutes vos données (grilles, classes, évalatuions...) seront supprimées','account.php?suppCompte='+<?= $_SESSION["auth"]->getId()?>)">Supprimer votre compte</button>
</form>
<?php
}
require './inc/footer.php'; ?>