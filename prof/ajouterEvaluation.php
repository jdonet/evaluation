<?php 
require './inc/header.php';

if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{
    //création des manager
    $eleveManager = new eleveManager(database::getDB());
    $noteManager = new noteManager(database::getDB());
    $critereManager = new critereManager(database::getDB());
    $classeManager = new classeManager(database::getDB());
    $grilleManager = new grilleManager(database::getDB());
    $evaluationManager = new evaluationManager(database::getDB());
    
    //insertion nouvelle evaluation
    if(isset($_POST["grille"])){
        $idCritere = $_POST['idCritere']??null;
        $nomCritere = $_POST['nomCritere']??null;
        $nbPointsCritere = $_POST['nbPointsCritere']??null;
        $grille = $grilleManager->get($_POST['grille']);

        //il faut archiver la précédente de cette classe
        $evaluationsActive = $evaluationManager->getList(" WHERE refClasse=".$_POST["classe"]." AND archiveEvaluation=0 AND refProf=".$_SESSION["auth"]->getId());
        if(!empty($evaluationsActive)){
            $evaluationsActive=$evaluationsActive[0];
            $evaluationsActive->setArchive(1);
            $evaluationManager->save($evaluationsActive);
        }
        //creation nouvelle
        $classe = $classeManager->get($_POST["classe"]);
        $eval = new evaluation($_POST["dateEval"],$_POST["nomEval"],0,$_SESSION["auth"],$classe);
        $eval->setId($evaluationManager->save($eval));
        //insertion des criteres à archiver
        for($i=0;$i<count($nomCritere);$i++){
            if(!empty($nbPointsCritere[$i]) && !empty($nomCritere[$i]))
                $critere = new critere($nomCritere[$i],$nbPointsCritere[$i],1,null,$eval);
            
            $critereManager->save($critere);
        }
        $_SESSION["flash"]['success'] = "Insertion effectuée";
        header("Location: index.php");

    }


    $tabEval = $evaluationManager->getList("WHERE refProf= ".$_SESSION["auth"]->getId());
    //recup liste éleves
    $tabClasses = $classeManager->getList(" WHERE refProf=".$_SESSION["auth"]->getId());

    if(count($tabClasses)==0){ //s'il n'a pas de classe
        header("Location: gestionClasses.php");
        exit();
    }
    //recup liste éleves de ce prof
    $tabEleves = $eleveManager->getList("WHERE classeEleve = ". $tabClasses[0]->getId());

    
    //recup liste criteres de ce prof
    $tabCriteres = $critereManager->getList(",eval_grille WHERE refEvaluation IS NULL AND refGrille=idGrille AND refProf=".$_SESSION["auth"]->getId());
    foreach ($tabCriteres as $critere) {
    ?>
    <script> ajouterCritere(<?=$critere->getId()?>,'<?=$critere->getNom()?>',<?=$critere->getPointMax()?>,<?=$critere->getGrille()->getId()?>) </script>
    <?php
    }

    //recup liste grilles de ce prof
    $tabGrilles = $grilleManager->getList(" WHERE refProf=".$_SESSION["auth"]->getId());
    
    //recup liste evaluations de ce prof
    $tabEvaluationsActives = $evaluationManager->getList(" WHERE archiveEvaluation=0 AND refProf=".$_SESSION["auth"]->getId());
    $tabEvaluationsArchives = $evaluationManager->getList(" WHERE archiveEvaluation=1 AND refProf=".$_SESSION["auth"]->getId());

    if (sizeof($tabCriteres)==0 or sizeof($tabEleves) ==0 or sizeof($tabClasses)==0 or sizeof($tabGrilles)==0  ){
        if (sizeof($tabClasses)==0  ){
            echo "<h3>Merci de saisir des classes</h3>";
        }
        if (sizeof($tabEleves) ==0){
            echo "<h3>Merci de saisir des élèves</h3>";
        }
        if (sizeof($tabCriteres)==0){
            echo "<h3>Merci de saisir des critères</h3>";
        }
        if (sizeof($tabGrilles)==0){
            echo "<h3>Merci de saisir des grilles</h3>";
        }
    }else{
        
        
        ?>


        <h2>Gestion des évaluations</h2>
        <div>
            <h4>Nouvelle évaluation</h4>
            <div class="alert alert-info" role="alert">
                L'ajout d'une nouvelle évaluation pour une classe archivera automatiquement l'évaluation active de cette même classe.
            </div>
            <form method="POST">
            <table class="table">
                    <tr>
                        <td>Date <input name="dateEval" type="date" value="<?= date("Y-m-d")?>" placeholder="<?= date("Y-m-d")?>" class="form-control"></td>
                        <td>Nom <input name="nomEval" type="text" placeholder="Nom de l'évaluation" required class="form-control"></td>
                        <td>Classe 
                            <select name="classe" class="form-control">
                            <?php 
                            foreach ($tabClasses as $classe){
                                echo '<option value="'.$classe->getId().'">'.$classe->getNom().'</option>';
                            }
                            ?>
                            </select>
                        </td>
                    </tr>
                    <tr>                        
                        <td>Basé sur la grille
                            <select name="grille" id="grille" onchange="filtreCriteres();"class="form-control">
                            <?php 
                            foreach ($tabGrilles as $grille){
                                if($grille->getActive()==1)
                                    echo '<option selected value="'.$grille->getId().'">'.$grille->getNom().'</option>';
                            else
                                echo '<option value="'.$grille->getId().'">'.$grille->getNom().'</option>';
                            }
                            ?>
                            </select>
                        </td>
                        <td colspan="2">Critères
                            <table id="tableCriteres">
                                <thead>
                                    <tr>
                                    <th scope="col">Nom</th>
                                    <th scope="col">Nb points</th>
                                    <th scope="col">Suppression</th>
                                    </tr>
                                </thead>
                                <tbody id="rowsCritere">
                                
                                </tbody>
                                    <tr>
                                        <td colspan="3"><input type="button" value="ajouter critère" onclick ="ajouteLigne()" class="form-control"></td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><button type="submit"class="form-control"> Ajouter une évaluation</button></td>
                    </tr>
                </table>
            </form>
         </div>
<script>
filtreCriteres();
</script>
        <?php
    }
}
require './inc/footer.php'; ?>