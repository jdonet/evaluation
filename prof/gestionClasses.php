<?php
require './inc/header.php';

//création des manager
$classeManager = new classeManager(database::getDB());
$eleveManager = new eleveManager(database::getDB());


if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["danger"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{


//si on veut supprimer une classe
if(isset($_GET["suppClasse"])){
    $classeSup = $classeManager->getList("WHERE refProf=".$_SESSION["auth"]->getId() ." AND idClasse = ".$_GET["suppClasse"])[0];
    $classeManager->delete($classeSup);
    $_SESSION["flash"]['success'] = "Suppression effectuée";
    header("Location: gestionClasses.php");
}

//si on veut ajouter ou modifier une classe
if(isset($_POST['modifierIdClasse']) && isset($_POST['modifierNomClasse']) && !empty($_POST['modifierNomClasse'])){
    $nomEleve = $_POST['nomEleve']??array();
    if(count($nomEleve)<=0){
        //on supprime la classe si elle ne contient aucun élève ??
        $classeSup = $classeManager->getList("WHERE refProf=".$_SESSION["auth"]->getId() ." AND idClasse = ".$_POST['modifierIdClasse'])[0];
        $classeManager->delete($classeSup);
        $_SESSION["flash"]['danger'] = "Erreur: Une classe doit comporter au moins un élève";
        header("Location: gestionClasses.php");    
    }else{
        $idEleve = $_POST['idEleve']??array();
        $prenomEleve = $_POST['prenomEleve']??array();
        $loginEleve = $_POST['loginEleve']??array();
        $passEleve = $_POST['passEleve']??array();
        $actifEleve = $_POST['actifEleve']??array();

        //modification de l'ancienne classe
        $classe = new classe($_POST["modifierNomClasse"],$_SESSION["auth"]);
        !empty($_POST['modifierIdClasse'])?$classe->setId($_POST['modifierIdClasse']):""; //si on a une modif, pas un ajout
        $classeManager->save($classe);

        if(!empty($_POST['modifierIdClasse'])){
            //suppression des eleves effacés (ceux de la classe avant qui n'y sont plus maintenant)
            $lesElevesForm=array();
            $lesElevesAjoutes=array();
            $lesElevesModifies=array();
            $lesElevesASupprimer=array();
            $lesEleves = $eleveManager->getList("WHERE classeEleve=".$_POST["modifierIdClasse"]);

            //recupération des données du formulaire
            for($i=0;$i<count($idEleve);$i++){
                $eleve = new eleve($nomEleve[$i],$prenomEleve[$i],$actifEleve[$i],$loginEleve[$i],$passEleve[$i],$classe);
                //si l'eleve avait un id
                if (isset($idEleve[$i]) && $idEleve[$i]!=""){
                    $eleve->setId($idEleve[$i]);
                    $lesElevesForm[] = $eleve;
                    $lesElevesModifies[] = $eleve;
                }else{
                    $lesElevesForm[] = $eleve;
                    $lesElevesAjoutes[] = $eleve;
                }
            }
            for($i=0;$i<count($lesEleves);$i++){
                if (!in_array($lesEleves[$i], $lesElevesForm)) 
                    $lesElevesASupprimer[] = $lesEleves[$i];
            }
            //suppression de ceux qui n'y sont plus
            for($i=0;$i<count($lesElevesASupprimer);$i++){
                $eleveManager->delete($lesElevesASupprimer[$i]);
            }

            $erreurMail=0;
            $cptEleves=0;
            //ajout/modif des autres eleves
            for($i=0;$i<count($lesElevesForm);$i++){
                //maj liste élèves
                $login=$lesElevesForm[$i]->getLogin();
                if($eleveManager->existeLogin($login) && $eleveManager->getIdFromLogin($login)!=$lesElevesForm[$i]->getId()){
                    $erreurMail++;
                }else{
                    $eleveManager->save($lesElevesForm[$i]);
                    $cptEleves++;
                }
            }
            if($cptEleves==0){ //si aucun élève créé, on supprime la classe
                $classeManager->delete($classe);
            }
            

        }
        

        if ($erreurMail>0){
            $_SESSION["flash"]['danger'] = $erreurMail." compte(s) élève non créé(s) car ce mail existe déjà en BDD";
            header("Location: gestionClasses.php");
        }else{
            $_SESSION["flash"]['success'] = "Modification effectuée";
            header("Location: gestionClasses.php");
        }
        
    }
    ?>
    <?php

    
}

    
//recup liste classes
$tabClasses = $classeManager->getList("WHERE refProf=".$_SESSION["auth"]->getId());


    
?>
<h2>Gestion des classes</h2>
<div class="form-inline">
       <div class="card-group">
            <?php 
            $i=0;
            foreach ($tabClasses as $classe){
                if($i%3==0){
                    echo "</div><div class='card-group'>";
                }
                $i++;
                ?>
                <form method="POST" action="editerClasse.php">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="../img/classe.png" alt="Choisir une classe" onclick="this.form.submit()">
                        <div class="card-body">
                            <input type="hidden" name="idClasse" value="<?php  echo  $classe->getId();?>">
                            <h5 class="card-title"><?= $classe->getNom() ?></h5>
                            <button class="btn btn-outline-dark" type="submit">Modifier</button>
                            <button class="btn btn-outline-dark" type="button" onclick="supprimer('cette classe','gestionClasses.php?suppClasse='+<?= $classe->getId()?>)">Supprimer</button>
                        </div>
                    </div>
                </form>
                <?php
            }
            
            if($i%3 ==0){
                ?>
                </div><div class='card-group'>
                <form method="POST" action="editerClasse.php">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="../img/ajouter.png" alt="Ajouter une classe">
                        <div class="card-body">
                            <h5 class="card-title">Ajouter une classe</h5>
                            <button class="btn btn-outline-dark" type="submit">Ajouter</button>
                        </div>
                    </div>
                </form>
                <div class="card" style="width: 18rem;">
                </div>
                <div class="card" style="width: 18rem;">
                </div>
                <?php
            }else if($i%3 ==1){
                ?>
                <form method="POST" action="editerClasse.php">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="../img/ajouter.png" alt="Ajouter une classe">
                        <div class="card-body">
                            <h5 class="card-title">Ajouter une classe</h5>
                            <button class="btn btn-outline-dark" type="submit">Ajouter</button>
                        </div>
                    </div>
                </form>
                <div class="card" style="width: 18rem;">
                </div>
                <?php
            }else{
                ?>
                <form method="POST" action="editerClasse.php">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="../img/ajouter.png" alt="Ajouter une classe">
                        <div class="card-body">
                            <h5 class="card-title">Ajouter une classe</h5>
                            <button class="btn btn-outline-dark" type="submit">Ajouter</button>
                        </div>
                    </div>
                </form>
                <?php
            }?>
        </div>
 </div>
           

<?php

}
require './inc/footer.php'; ?>