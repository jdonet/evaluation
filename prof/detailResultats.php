<?php 
require './inc/header.php';


if(!isset($_SESSION["auth"])){
    $_SESSION["flash"]["error"]="Vous devez vous connecter pour accéder à cette page";
    header('Location: login.php');
    exit();
}else{


    //création des manager
    $eleveManager = new eleveManager(database::getDB());
    $noteManager = new noteManager(database::getDB());
    $critereManager = new critereManager(database::getDB());
    $classeManager = new classeManager(database::getDB());
    $evaluationManager = new evaluationManager(database::getDB());

    
   
    //recup liste éleves de ce prof
    $tabClasses = $classeManager->getList("WHERE refProf=".$_SESSION["auth"]->getId());
    $evaluationChoisie = isset($_GET["evaluation"])?$evaluationManager->get($_GET["evaluation"]):(isset($_POST["evaluation"])?$evaluationManager->get($_POST["evaluation"]):null);
    $eleveChoisi = isset($_GET["eleve"])?$eleveManager->get($_GET["eleve"]):(isset($_POST["eleve"])?$eleveManager->get($_POST["eleve"]):null);
    
    if(sizeof($tabClasses)!=0 && !is_null($evaluationChoisie) && in_array($evaluationChoisie->getClasse(),$tabClasses)
    && !is_null($eleveChoisi) && in_array($eleveChoisi->getClasse(),$tabClasses)){
        
        //recup liste éleves de ce prof
        $tabEleves = $eleveManager->getList("WHERE classeEleve=".$evaluationChoisie->getClasse()->getId());
        //recup liste criteres de la grille active de ce prof
        $tabCriteres = $critereManager->getList("WHERE refEvaluation=".$evaluationChoisie->getId());
  
        
        $d = new DateTime($evaluationChoisie->getDate());
        ?>
        <h2><?= $evaluationChoisie->getNom()?></h2>
        <h4>Notes de <?=$eleveChoisi->getPrenom()?> <?=$eleveChoisi->getNom()?> - <?= $d->format("d/m/Y");?></h4>
        <p>Détail des notes reçues par l'élève sur chaque critère</p>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Votant</th>
                    <?php
                    foreach ($tabCriteres as $critere){
                        echo "<th scope='col'>".$critere->getNom() ."</th>";
                    }
                    ?>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tboby>
            <?php 
            //si on veut voir les résultats
            foreach ($tabEleves as $noteur){
                $total=0;
                ?>
                <tr>
                    <td><?=$noteur->getNom()?></td>
                    <?php
                    foreach ($tabCriteres as $critere){
                        $note = $critereManager->getNoteAttribuee($critere,$noteur,$eleveChoisi);
                        is_numeric($note)?$total+=$note:"";
                        echo "<td>".$note."</td>";
                    }
                    ?>
                    <td><?=$total?></td>
                </tr>
            <?php 
            }?>
            </tboby>
        </table>
        <form method="POST" action="resultats.php?evaluation=<?=$evaluationChoisie->getId()?>">
            <button type="submit"class="form-control"> Retour</button>
        </form>
    <?php
    }else{
        if(sizeof($tabClasses)==0)
            echo "<h3>Merci de choisir une évaluation pour accéder aux résultats </h3>";
        else
            echo "<h3>Merci de choisir une évaluation qui vous appartient pour accéder aux résultats </h3>";
    }
        ?>

<?php
}
require './inc/footer.php'; ?>     