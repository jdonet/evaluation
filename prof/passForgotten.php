<?php 
require './inc/header.php';

if(!empty($_POST) and !empty($_POST["username"])){
    $profManager = new profManager(database::getDB());
    $user= $profManager->verifLogin($_POST['username']);
    //si utilisateur trouvé
    if ($user != false){
        $token = str_random(60);
        $user->setToken($token); // on réinit son token
        $id= $profManager->save($user);;
        mail($_POST['username'], "Réinitialisation mot de passe EvalOral", "Afin de réinitialiser votre mot de passe, merci de cliquer sur ce lien \n\n http://evaluation.donet.yt/prof/reinit.php?id=".$id."&token=".$token);
        $_SESSION["flash"]['success'] = "Un email de confirmation vous a été envoyé pour réinitialiser votre mot de passe";
        header('Location: login.php');
        exit();    
    }else{
        $_SESSION["flash"]["danger"]="Email introuvable";
        header('Location: passForgotten.php');
        exit();
    }
    
}
?>

<h1>Fonction de rappel de mot de passe</h1>

<form method="POST">
    <div class="form-group"> 
        <label for="username">Adresse email</label>
        <input type="email" class="form-control" name="username"/>
    </div>        
    <button type="submit" class="btn btn-primary">Réinitialiser le mot de passe</button>    
</form>
<?php require './inc/footer.php'; ?>
