var criteres =[];

function supprimer(objet,url){
    if(confirm("Voulez-vous vraiment supprimer "+objet+" ?")){
        window.location = url;
    }
}
function ajouteEleve(id="",nom="",prenom="",mail="",actif=0,pass=""){
    // création d'une nouvelle ligne dans la table
    var table=document.getElementById("tableEleves");
    var maLigne = table.insertRow(table.rows.length-1); //insérer avant les  dernières lignes
    var cellules="";
    // définition du contenu de cette ligne
    cellules +="<tr><td><input type=\"hidden\" name=\"idEleve[]\" value=\""+id+"\"></input><input type=\"text\" name=\"nomEleve[]\" value=\""+nom+"\" placeholder=\"nom de l'élève\" required class=\"form-control\" size=\"10\"></td>"
    cellules +="<td><input type=\"text\" name=\"prenomEleve[]\" value=\""+prenom+"\" placeholder=\"prénom de l'élève\" class=\"form-control\" size=\"10\"></td>"
    cellules +="<td><input type=\"email\" name=\"loginEleve[]\" value=\""+mail+"\"placeholder=\"email de l'élève\" required class=\"form-control\" size=\"10\"></td>"
    cellules += "<input type=\"hidden\" name=\"actifEleve[]\" value=\""+actif+"\">"
    cellules +="<td><input type=\"text\" name=\"passEleve[]\" value=\""+pass+"\"placeholder=\"pass de l'élève\" required class=\"form-control\" size=\"10\"></td>"
    cellules +="<td><button type=\"button\" onclick=\"supprimeLigne(this.parentNode.parentNode);\"><img alt='supprimer eleve' width='20px' src='../img/supprimer.jpg'></button></td>"
    cellules +="</tr>";        
    maLigne.innerHTML=cellules;
}

function ajouteLigne(id="",nom="",nbPoints=0,idGrille=""){
    // création d'une nouvelle ligne dans la table
    var table=document.getElementById("tableCriteres");
    var maLigne = table.insertRow(table.rows.length-1); //insérer avant les  dernières lignes
    var cellules="";
    // définition du contenu de cette ligne
    cellules +="<tr><td><input required type=\"hidden\" name=\"idCritere[]\" value=\""+id+"\"><input required type=\"hidden\" name=\"idGrille[]\" value=\""+idGrille+"\">"
    cellules +="<input type=\"text\" name=\"nomCritere[]\" value=\""+nom+"\" placeholder=\"nom du critère\" required class=\"form-control\" size=\"10\"></td>"
    cellules +="<td><input required type=\"number\" min=\"1\"  name=\"nbPointsCritere[]\" value=\""+nbPoints+"\" placeholder=\"nombre de points du critère\" class=\"form-control\" size=\"10\"></td>"
    cellules +="<td><button type=\"button\" onclick=\"supprimeLigne(this.parentNode.parentNode);\"><img alt='supprimer critere' width='20px' src='../img/supprimer.jpg'></button></td>"
    cellules +="</tr>";        
    maLigne.innerHTML=cellules;
}


function supprimeLigne(tr) {
    //On supprime l'élément elt du DOM
    tr.remove();
}

function ajouterCritere(id,nom,nbPoints,idGrille) {
    var element = {};
    element.id = id;
    element.nom = nom;
    element.nbPoints = nbPoints;
    element.idGrille = idGrille;
    criteres.push(element);
}

function filtreCriteres() {
    var grille = document.getElementById("grille").value;
    var old_tbody = document.getElementById("rowsCritere");
    
    //supprimer les anciennes lignes
    var new_tbody = document.createElement('tbody');
    new_tbody.setAttribute("id", "rowsCritere");
    old_tbody.parentNode.replaceChild(new_tbody, old_tbody);

    criteres.forEach(function(element) {
        if(element.idGrille == grille )
            ajouteLigne(element.id,element.nom,element.nbPoints)
      });    

}